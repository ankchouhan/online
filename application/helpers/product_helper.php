<?php
  function get_category_product($id)
  {
    $CI =& get_instance();
    $fetch_data = $CI->db->select('category.category_name,product.*')
            ->from('product')
            ->where(['category_id'=>$id,'product.status'=>"0"])
            ->join('category','product.category_id = category.id')
            ->limit('6')
            ->order_by('id','desc')
            ->get();
    if($fetch_data->num_rows()>0)
    {
      return $fetch_data->result();
    }else {
      return $fetch_data->result();
    }
  }
  function get_category_product_name($id)
  {
    $CI =& get_instance();
    $fetch_data = $CI->db->select('category.category_name,product.*')
            ->from('product')
            ->where(['category_id'=>$id])
            ->join('category','product.category_id = category.id')
            ->get();
    if($fetch_data->num_rows()>0)
    {
      return $fetch_data->result();
    }else {
      return $fetch_data->result();
    }
  }
  function get_category_details($id)
  {
    $CI =& get_instance();
    $fetch_rec =$CI->db->get_where('category',['id'=>$id]);
    if($fetch_rec->num_rows()>0){
      return $fetch_rec->result();
    }else {
      return $fetch_rec->result();
    }
  }
  function get_product_details($id)
  {
    $CI =& get_instance();
    $fetch_rec =$CI->db->get_where('product',['id'=>$id]);
    if($fetch_rec->num_rows()>0){
      return $fetch_rec->result();
    }else {
      return $fetch_rec->result();
    }
  }
    function get_product_by_order_id($id)
    {
      $CI =& get_instance();
      $fetch_rec = $CI->db->get_where('order_product',['order_id'=>$id]);
      if($fetch_rec->num_rows()>0)
      {
        return $fetch_rec->result();
      }else {
        return $fetch_rec->result();
      }
    }
    function get_all_product($id)
    {
      $CI =& get_instance();
      $fetch_rec = $CI->db->get_where('product',['category_id'=>$id]);
      if($fetch_rec->num_rows()>0)
      {
        return $fetch_rec->result();
      }else {
        return $fetch_rec->result();
      }
    }
    function get_all_customer($order_date)
      {
        $CI =& get_instance();
        $fetch_rec = $CI->db->get_where('orders',['order_date'=>$order_date,'order_status'=>"Delivered"]);
        if($fetch_rec->num_rows()>0)
        {
          return $fetch_rec->result();
        }else {
          return $fetch_rec->result();
        }
      }
 ?>
