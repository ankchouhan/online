<!-- include jquery js file  -->
<script src="<?= base_url('assets/jquery/jquery.js');?>"></script>
<!-- include materialize js file -->
<script src="<?= base_url('assets/materialize/js/materialize.js');?>"></script>
<!-- custom js file inclue -->
<script type="text/javascript">
  $(document).ready(function(){
    // dropdown trigger
    $('.dropdown-trigger').dropdown({
      coverTrigger:false,
      hover:true,
    });

    // image slider
    $('.carousel.carousel-slider').carousel({
      fullWidth: true,
      indicators: true
    });
  });
</script>
