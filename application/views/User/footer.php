<!-- footer section start -->
<nav style="color:black;">
</nav>
  <footer class="page-footer" style="color:black;">
      <div class="row" style="color:black;">
          <div class="col l4 m6 s12">
            <h5 style="font-weight:700;">About Us</h5>
            <p style="text-align:justify; padding-right:30px;font-weight:500;">To make your document look professionally produced, Word provides header,
              footer, cover page, and text box designs that complement each other.</p><p style="text-align:justify;font-weight:500;padding-right:30px;">you can add a matching cover page,
              header, and sidebar. Click Insert and then choose the elements you want from the different galleries.</p>
          </div>
        <div class="col l4 s12" style="color:black!important;padding-left:50px;">
          <h5 style="font-weight:700">Links</h5>
          <ul>
            <li style="margin-top:5px;font-weight:500;"><a style="color:black!important;" href="#!">Company Profile</a></li>
            <li style="margin-top:5px;font-weight:500;"><a style="color:black!important;" href="#!">Policies</a></li>
            <li style="margin-top:5px;font-weight:500;"><a style="color:black!important;" href="#!">User Login</a></li>
            <li style="margin-top:5px;font-weight:500;"><a style="color:black!important;" href="#!">Register</a></li>
          </ul>
        </div>
        <div class="col l4 m6 s12" >
          <h5 style="font-weight:700">Contact information</h5>
          <p><span class="fa fa-map-marker-alt">&nbsp;&nbsp;12,Namdarpura Ujjain-456006</span></p>
          <p> <span class="fa fa-phone">&nbsp;&nbsp;+91 - 8225994767, 8319193774</span></p>
          <p><span class="fa fa-envelope">&nbsp;&nbsp;ankitchouhan654@gmail.com</span></p>
          <p> <span class="fa fa-globe">&nbsp;&nbsp;www.ankitsoftservices.com</span></p>
          <!-- social media icon section start -->
          <ul id="set_social_icon">
            <li><a  href="https://www.facebook.com/profile.php?id=100008759354878" target="_blank"><span class="fab fa-facebook"></span></a></li>
            <li><a  href="https://www.instagram.com/a_n_k_i_t_chouhan__/" target="_blank"><span class="fab fa-instagram-square"></span></a></li>
            <li><a  href="#"><span class="fab fa-twitter"></span></a></li>
            <li><a  href="#"><span class="fab fa-youtube"></span></a></li>
            <li><a  href=""><span class="fab fa-google"></span></a></li>
          </ul>
          <!-- social media icon section end -->
        </div>
      </div>
    <div class="footer-copyright" style="color:#eae7dc;background:black;">
      <div class="container">
      © 2020 Copyright Text
      <a class="right"style="color:#eae7dc!important;font-weight:500;" href="#!">Devleop By : Ankit Chouhan</a>
      </div>
    </div>
  </footer>
  <br>
<!-- footer section end -->
