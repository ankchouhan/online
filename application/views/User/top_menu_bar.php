<!-- topbar section start -->
<div id="topbar">
  <h6 style="font-size:14px;margin-top:5px;padding-left:15px;font-weight:500;"> <span class="fa fa-mobile-alt"></span>&nbsp;+91-8225994767&nbsp;&nbsp;<span class="fa fa-envelope-open-text">
  </span>&nbsp;info@Onlineshop.com <span class="right"><a style="color:black;padding-right:15px;" class="dropdown-trigger" data-target="my_account_dropdown"><span class="far fa-user"></span>&nbsp;My Account</a></span></h6>
  <!-- my account dropdown swction start -->
  <ul class="dropdown-content" id="my_account_dropdown">
    <li><a href="<?= base_url('Home/User_Signup');?>" class="waves-effect"><span class="fa fa-user-plus"></span>&nbsp;Register</a> </li>
    <li><a href="<?= base_url('Home/User_Signin');?>" class="waves-effect"><span class="fa fa-sign-in-alt"></span>&nbsp;LogIn</a> </li>
  </ul>
  <!-- my account dropdown swction end -->
</div>
<!-- topbar section end -->
<!-- search bar section start -->
<div id="search_bar">
  <div class="row" style="margin-bottom:0px;">
    <div class="col l3 m3 s12">
        <h6 style="margin-top:25px;"><a href="<?=base_url('Home/index');?>" class="brand-logo" id="logo"><span class="fab fa-shopware"></span>nline Shop</a> </h6>
    </div>
    <div class="col l6 m6 s12">
      <!-- search product form section start -->
      <ul id="search_form">
        <li>
          <input type="text" name="search" value="" id="search" placeholder="Search Your product">
        </li>
        <li>
          <button type="submit" name="button" class="btn waves-effect waves-light" style="box-shadow:none;background:black;
          text-transform:capitalize;height:37px;font-weight:500;">Search Now</button>
        </li>
      </ul>
      <!-- search product form section end -->
    </div>
    <div class="col l3 m3 s12">
        <h6 style="color:black;font-size:16px;text-align:center;font-weight:500;margin-top:20px;margin-right:15px;line-height:20px;">
        <a style="color:black;"href="<?= base_url('Home/Carts');?>"> <span class="fa fa-shopping-cart"></span>&nbsp;Shopping Cart<br>0 Items - 
        <span class="fa fa-rupee-sign"></span>0</a></h6>
    </div>
  </div>
</div>
<!-- search bar section end -->
<!-- menu bar section start -->
<nav>
  <div class="nav-wrapper">
    <!-- left side menu section start -->
      <ul class="left">
        <li><a href="#">Home</a></li>
        <li><a href="#">Company Profile</a></li>
        <li><a href="#">Our Policies</a></li>
        <li><a href="#">Offers</a></li>
        <li><a href="#">Contact Us</a></li>
      </ul>
    <!-- left side menu section end -->
  </div>
</nav>
<!-- menu bar section end -->
