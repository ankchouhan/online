<!-- Add shortcut icon -->
<link rel="shortcut icon"  href="<?=base_url('assets/image/logo.jpg');?>" >
<!-- include Materialize css file -->
<?= link_tag('assets/materialize/css/materialize.css');?>
<!-- include font awesome file -->
<?= link_tag('assets/font/css/all.css');?>
<!-- custom css file -->
<style media="screen">
  body{
    background:rgb(234 231 220 / 26%);
  }
  #my_account_dropdown{
    width:12%!important;
    padding-bottom:10px;
    padding-top:10px;
  }
  #my_account_dropdown li a{
    color:gray;
    font-size:14px;
  }
  #topbar{
    background: #eae7dc;
    padding: 5px;
  }
  #search_bar{
    background:#eae7dc;
  }
  #search_bar #logo{
    font-size:30px;
    font-weight:700;
    color:black;
  }
  #search_form{
    display:flex;
  }
  #search_form :focus{
    border: 1px solid black;
  }
  #search_form li:first-child{
    width:400px;
  }
  #search{
    background: white;
    padding-left: 10px;
    padding-right: 10px;
    box-shadow: none;
    box-sizing: border-box;
    height:37px;
    border-bottom: none;
    border-radius: 2px;
  }
  nav{
    background: black;
    height: 40px;
    line-height: 40px;
    box-shadow: none;
  }
  footer{
    background:#eae7dc!important;
    color:black!important;
  }
  #set_social_icon li{
    display: inline;
  }
  #set_social_icon li a{
    font-size:25px;
    color:black;
    margin-left: 5px;
  }
</style>
