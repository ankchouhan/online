<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Admin Login - Online Shop</title>
    <!-- css file include start -->
    <?php $this->load->view('Home/css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <?php include('custom_css.php');?>
  </head>
  <body>
    <!-- body section start -->
    <!-- nav and side menu section start -->
    <?php include('nav.php');?>
    <!-- nav and side menu section end -->
    <!-- title section start -->
    <h5 style="margin-left:15px;color:#453a50fc;margin-top:20px;">Admin Control</h5>
    <p style="margin-left:15px;margin-top:-8px;color:silver;margin-bottom:20px;">Welcome to Online Shopping Dashboard</p>
    <!-- title section end -->
    <!-- main section start -->
    <!-- 4 card section start -->
    <div class="row" style="margin-top:15px;margin-bottom:0px;">
      <div class="col l3 m4 s12">
        <div class="card hoverable">
          <div class="card-content">
            <div class="row" style="margin-bottom:10px;">
              <div class="col l6 m6 s12">
                <h6 style="font-size:15px;font-weight:500;"> <a href="<?= base_url('Admin/Manage_Orders');?>" style="color:black;">Orders</a></h6>
              </div>
              <div class="col l6 m6 s12">
                <h6 class="dropdown-trigger" data-target="order_dropdown" style="cursor:pointer;margin-right:11.5px;"> <span class="right">
                  <span class="fa fa-ellipsis-v"></span></span></h6>
              </div>
            </div>
            <h5 style="margin-top:0px;color:lightseagreen;"> <b><span id="show_order">0</span></b> <span class="right"> <span class="fa fa-shopping-cart black-text"></span> </span> </h5>
            <h6 style="color:silver;font-size:14px;"><span id="order_show">Life-Time</span> </h6>
            <!-- order dropdkown section start -->
            <ul class="dropdown-content" id="order_dropdown">
              <li> <a href="#!" onclick="count_order('today')">Today Orders</a> </li>
              <li> <a href="#!" onclick="count_order('yesterday')">Previous Day Orders</a> </li>
              <li> <a href="#!" onclick="count_order('last_30')">Last 30 Days Orders</a> </li>
              <div class="divider"></div>
              <li> <a href="#!" onclick="count_order('all')">All Orders</a> </li>
            </ul>
            <!-- order dropdkown section end -->
          </div>
        </div>
      </div>
      <div class="col l3 m4 s12">
        <div class="card hoverable" style="width:100%;">
          <div class="card-content">
            <div class="row" style="margin-bottom:10px;">
              <div class="col l6 m6 s12">
                <h6 style="font-size:15px;font-weight:500;"> <a href="<?= base_url('Admin/Customers');?>" style="color:black;">Customers</a></h6>
              </div>
              <div class="col l6 m6 s12">
                <h6 style="cursor:pointer;margin-right:11.5px;"> <span class="right">
                  <span class="fa fa-ellipsis-v"></span></span></h6>
              </div>
            </div>
            <h5 style="margin-top:0px;color:lightseagreen;"> <b> <span id="show_seller_number"><?= count($customer);?></span> </b> <span class="right"> <span class="fa fa-user-friends black-text"></span> </span> </h5>
            <h6 style="color:silver;font-size:14px;">All Customers</h6>
            <!-- order dropdkown section start -->
            <ul class="dropdown-content" id="seller_dropdown">
              <li> <a href="#">Today Orders</a> </li>
              <li> <a href="#">Previous Day Orders</a> </li>
              <li> <a href="#">Last 30 Days Orders</a> </li>
              <div class="divider"></div>
              <li> <a href="#">All Orders</a> </li>
            </ul>
            <!-- order dropdkown section end -->
          </div>
        </div>
      </div>
      <div class="col l3 m4 s12">
        <div class="card hoverable">
          <div class="card-content">
            <div class="row" style="margin-bottom:10px;">
              <div class="col l6 m6 s12">
                <h6 style="font-size:15px;font-weight:500;"> <a href="<?= base_url('Admin/Manage_Category');?>" style="color:black;">Categories</a></h6>
              </div>
              <div class="col l6 m6 s12">
                <h6  style="cursor:pointer;margin-right:11.5px;"> <span class="right">
                  <span class="fa fa-ellipsis-v"></span></span></h6>
              </div>
            </div>
            <h5 style="margin-top:0px;color:lightseagreen;"> <b> <span id="show_seller_number"><?= count($category);?></span> </b> <span class="right"> <span class="fa fa-tag black-text"></span> </span> </h5>
            <h6 style="color:silver;font-size:14px;">All Categories</h6>
            <!-- order dropdkown section start -->
            <ul class="dropdown-content" id="income_dropdown">
              <li> <a href="#">Today Orders</a> </li>
              <li> <a href="#">Previous Day Orders</a> </li>
              <li> <a href="#">Last 30 Days Orders</a> </li>
              <div class="divider"></div>
              <li> <a href="#">All Orders</a> </li>
            </ul>
            <!-- order dropdkown section end -->
          </div>
        </div>
      </div>
      <div class="col l3 m4 s12">
        <div class="card hoverable">
          <div class="card-content">
            <div class="row" style="margin-bottom:10px;">
              <div class="col l6 m6 s12">
                <h6 style="font-size:15px;font-weight:500;"> <a href="<?= base_url('Admin/Manage_Product');?>" style="color:black;">Products</a></h6>
              </div>
              <div class="col l6 m6 s12">
                <h6  style="cursor:pointer;margin-right:11.5px;"> <span class="right">
                  <span class="fa fa-ellipsis-v"></span></span></h6>
              </div>
            </div>
            <h5 style="margin-top:0px;color:lightseagreen;"> <b> <span id="show_seller_number"><?= count($products)?></span> </b> <span class="right"> <span class="fa fa-cubes black-text"></span> </span> </h5>
            <h6 style="color:silver;font-size:14px;">All Products</h6>
            <!-- order dropdkown section start -->
            <ul class="dropdown-content" id="product_dropdown">
              <li> <a href="#">Today Orders</a> </li>
              <li> <a href="#">Previous Day Orders</a> </li>
              <li> <a href="#">Last 30 Days Orders</a> </li>
              <div class="divider"></div>
              <li> <a href="#">All Orders</a> </li>
            </ul>
            <!-- order dropdkown section end -->
          </div>
        </div>
      </div>
    </div>
    <!-- 4 card section end -->
    <!-- 2 card sectio  start -->
    <div class="row">
      <div class="col l7 m7 s12">
        <div class="card">
          <div class="card-content">
            <div id="chartContainer" style="height: 370px; width: 100%;"></div>
          </div>
        </div>
      </div>
      <div class="col l5 m5 s12">
        <div class="card">
          <div class="card-content">
            <h6 style="font-weight:500;margin-top:0px;">Top Product Sold</h6>
            <ul id="top_sold_products" class="hoverable" >
              <?php if(count($top_sold)):?>
                <?php foreach ($top_sold as $ts):?>
              <li  class="hoverable" >
                <h6 style="font-size:14px;font-weight:500;"><a target="_blank" href="<?=base_url('Home/Product_Details/'.$ts->id);?>" style="color:black;"><?= $ts->product_title;?></a></h6>
                <h6 style="font-size:15px;font-weight:500;">Price-<span class="fa fa-rupee-sign"></span>&nbsp;<?= number_format($ts->price);?><span class="right"> <b>Units - <?= $ts->count_sale;?></b></span> </h6>
              </li>
            <?php endforeach;
              else:?>
              <h6 style="text-align:center;color:gray;font-size:14px;">No Products in Top Sold</h6>
            <?php endif?>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- 2 card sectio  end -->
    <!-- main section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php $this->load->view('Home/js.php');?>
    <!-- include js file include end -->
    <!-- custom js file include -->
    <script type="text/javascript">
            window.onload = function () {
        var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light2",
        title:{
          text: "Last 7 Days Orders"
        },
          axisY: {
            includeZero: true
          },
        data: [{
          type: "column", //change type to bar, line, area, pie, etc
          //indexLabel: "{y}", //Shows y value on all Data Points
          indexLabelFontColor: "#5A5757",
              indexLabelFontSize: 16,
          indexLabelPlacement: "outside",
          dataPoints: [
            { label:'Today', y: <?= $chart['today_order'];?> },
            { label:'Yesterday', y: <?= $chart['yesterday_order'];?> },
            { label:'3rd Day', y: <?= $chart['third_order'];?> },
            { label:'4th Day', y: <?= $chart['fourth_order'];?> },
            { label:'5th Day', y: <?= $chart['fifth_order'];?> },
            { label:'6th Day', y: <?= $chart['six_order'];?> },
            { label:'7th Day', y: <?= $chart['seven_order'];?> },

          ]
        }]
        });
        chart.render();
        }
    </script>
    <script type="text/javascript">
    count_order();
    function count_order(type="all")
    {
        if(type == "all")
      {
        $('#order_show').text('Life-Time');
      }else if (type == "today") {
        $('#order_show').text('Today Orders');
      }else if (type == "yesterday") {
        $('#order_show').text('Yesterday Orders');
      }else if (type == "last_30") {
        $('#order_show').text('Last 30 Days');
      }else{
        $('#order_show').text('Life-Time');
      }
       $.ajax({
        type:'ajax',
        method:'get',
        url:"<?= base_url('Admin/count_order/');?>"+type,
        beforeSend:function(data){
        $('#show_order').text("Loading...");
        },
        success:function(data){
          $('#show_order').html(data);
        },
        error:function(){
          $('#show_order').text("0");

        },
      });
    }
    </script>
  </body>
</html>
