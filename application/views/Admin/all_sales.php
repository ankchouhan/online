<?php $this->load->helper('product'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Manage All Sales - Online Shop</title>
    <!-- css file include start -->
    <?php $this->load->view('Home/css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <?php include('custom_css.php');?>
    <style media="screen">
    #input_box{
      border:1px solid silver;
      padding:5px;
      height:35px;
      box-shadow: none;
    }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- nav and side menu section start -->
    <?php include('nav.php');?>
    <!-- nav and side menu section end -->
    <!-- Manage Category section start -->
    <div class="container">
      <div class="card">
        <div class="card-content" style="border-bottom:1px solid silver;padding-top:5px;">
          <h5><span style="font-weight:500;"> Manage All Sales </span><span class="right"><a href="#!" class="modal-trigger" data-target="customize_sales_modal" style="font-size:17px;font-weight:500;">Customize Sales</a></span> </h5>
          <h6><span style="font-size:14px;color:gray;font-weight:500;"><?= $start_date ;?> To</span><span style="font-size:14px;color:gray;font-weight:500;"id="end">&nbsp;<?= $end_date;?></span><span class="right"> <a href="<?= base_url('Admin/All_Sales');?>" style="color:red;font-weight:500;font-size:14px;">Reset</a> </span> </h6>
          <!-- customize sales modal section start -->
              <div class="modal" id="customize_sales_modal" style="width:40%;">
                <div class="modal-content" style="padding:10px;border-bottom:1px solid silver;">
                  <h6 style="font-weight:500;"><span class="fa fa-edit"></span> Customize Sales By Date</h6>
                </div>
                <div class="modal-content" style="padding:10px;">
                  <?= form_open('Admin/Search_Sales');?>
                  <div class="row" style="margin-bottom:0px;">
                    <div class="col l6 m6 s12">
                      <h6 style="font-size:14px;color:gray;margin-bottom:5px;">Start Date</h6>
                      <input type="date" name="start_date" class="start_date" id="input_box">
                    </div>
                    <div class="col l6 m6 s12">
                      <h6 style="font-size:14px;color:gray;margin-bottom:5px;">End Date</h6>
                      <input type="date" name="end_date" id="input_box">
                    </div>
                    <div class="col l12 m12 s12" style="margin-top:5px;">
                      <button type="submit" id="btn_date" name="button" class="btn waves-effect waves-light" style="background:green;text-transform:capitalize;">Search</button>
                      <button type="reset" name="button" class="btn waves-effect waves-light modal-close" style="background:black;text-transform:capitalize;">Cancel</button>
                    </div>
                  </div>
                  <?= form_close();?>
                </div>
              </div>
          <!-- customize sales modal section end -->
        </div>
        <div class="card-content" style="padding:0px;padding-right:10px">
          <table>
            <tr>
              <th style="text-align:center">Date</th>
              <th >Coustmers</th>
              <th style="text-align:right">Unit Sales</th>
              <th style="text-align:right">Amount</th>
            </tr>
            <?php
            $grand_total ="0";
            if(count($sales)):?>
              <?php foreach($sales as $sale):
                $grand_total += $sale['SUM(total_amount)'];?>
            <tr>
              <td style="text-align:center;vertical-align:top;font-size:14px;"><?= date("d-M-Y",strtotime($sale['order_date']));?></td>
              <td style="vertical-align:top;font-size:14px;"><?= $sale['COUNT(order_date)'];?>&nbsp;Customers<br/><br>
              <?php $total_customer = get_all_customer($sale['order_date']);?>
              <?php if(count($total_customer)):?>
                <?php foreach($total_customer as $tc):?>
              <span style="margin-top:15px;line-height:25px;"><i style="color:gray;">Sold By: <?=$tc->user_name;?></i></span><br>
            <?php endforeach;
              else:?>
              <i>Coustmer Not Found..</i>
            <?php endif;?>
            </td>
              <td style="text-align:right;vertical-align:top;font-size:14px;"><?= $sale['SUM(total_quantity)'];?>&nbsp;Units <br><br>
              <?php
              $total_quan ="";
               if(count($total_customer)):?>
                <?php foreach($total_customer as $tc): ?>
                <span style="margin-top:15px;"><i style="color:gray;"><?=$tc->total_quantity;?> Units</i></span><br>
                <?php endforeach;
                  else:?>
                  <i>Units Not Found..</i>
                <?php endif;?>
              </td>
              <td style="text-align:right;vertical-align:top;font-size:14px;"><span class="fa fa-rupee-sign" style="font-weight:0;"></span>&nbsp;<?= number_format($sale['SUM(total_amount)'],2,".",",");?>/-<br><br>
                <?php if(count($total_customer)):?>
                  <?php foreach($total_customer as $tc):?>
                    <span style="margin-top:15px;"><i style="color:gray;"><?= number_format($tc->total_amount,2,".",",");?>/-</i></span><br>
                  <?php endforeach;
                    else:?>
                    <i>Units Not Found..</i>
                  <?php endif;?>
              </td>
            </tr>
          <?php endforeach;
            else:?>
            <tr>
              <td colspan="4" style="text-align:center;color:gray;font-weight:500;">Sales Not Found..</td>
            </tr>
          <?php endif;?>
          <tr>
            <th colspan="3" style="text-align:right;">
              <h6 style="font-size:14px;font-weight:700;">Grand Total</h6>
            </th>
            <th>
              <h6 style="font-size:14px;font-weight:700;"><span class="right"><span class="fa fa-rupee-sign"></span> <?= number_format($grand_total,2,".",",");?></span></h6>
            </th>
          </tr>
          </table>
        </div>
      </div>
    </div>
    <!-- Manage Category section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php $this->load->view('Home/js.php');?>
    <!-- include js file include end -->
    <!-- custom js file include -->
    <script type="text/javascript">
      $('#btn_date').on('click', function(){
        var start = $('input[name="start_date"]').val();
        $('#start').html(start);
        });
    </script>
  </body>
</html>
