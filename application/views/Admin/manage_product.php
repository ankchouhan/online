<?php $this->load->helper('product'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Manage Products - Online Shop</title>
    <!-- css file include start -->
    <?php $this->load->view('Home/css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <?php include('custom_css.php');?>
    <style media="screen">
      .container{
        width:120%;
      }
      .btn-flat:hover{
        background: black;
        color:white;
      }
      #category_image{
        width:40px;
        height:40px;
        border-radius: 100%;
        border:1px solid silver;
      }
      .action_dropdown li a{
        color:gray;
        font-size:14px;
        font-weight:500;
      }
      #search_category{
        box-shadow: none;
        height:30px;
        padding-left: 10px;
        padding-right: 10px;
        border:none;
        padding-top:4px;
        line-height: 40px;
      }
      #category_filter {
        width: 13%!important;
        padding-top:8px;
        padding-bottom: 8px;
      }
      #category_filter li a{
        color:gray;
        font-size: 14px;
        font-weight:500;
      }
      #ajax a{
        color:black;
        font-weight: 500;
        border: 1px solid black;
        padding:5px 8px;
        margin-left: 5px;
      }
      #ajax strong{
        color:white;
        background: black;
        font-weight: 500;
        border: 1px solid black;
        padding:5px 8px;
        margin-left: 5px;
      }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- nav and side menu section start -->
    <?php include('nav.php');?>
    <!-- nav and side menu section end -->
    <!-- Manage Category section start -->
    <div class="container">
      <div class="card">
        <div class="card-content" style="border-bottom:1px solid silver;padding:8px;" >
          <h5 style="font-weight:500;">Manage Products</h5>
          <div class="row" style="margin-bottom:0px;">
            <div class="col l6 m6 s12">
              <ul>
                <li style="display:flex;border:1px solid silver;border-radius:60px;width:80%;" >
                  <input type="text" name="search_category" id="search_category" autocomplete="off" placeholder="Enter Product Name" required><span class="fa fa-search" style="background:none;border:none;margin-right:12px;margin-top:13px;"></span></input>
                </li>
                <li>
                  <!-- <button type="button" name="button" class="btn waves-effect waves-light" style="background:black;text-transform:capitalize;">Search</button> -->
                </li>
              </ul>
            </div>
            <div class="col l6 m6 s12">
              <span class="right">
                <button type="button" class="btn btn-flat btn waves-effect waves-light dropdown-trigger"  data-target="category_filter" style="text-transform:capitalize;background:black;color:white;margin-top:20px;" name="button"><span class="fa fa-filter"></span> Filter</button>
              </span>
              <!-- dropdown filter start -->
              <ul class="dropdown-content" id="category_filter">
                <li><a href="#" value="new_category" class="waves-effect">New Product</a> </li>
                <li><a href="#" value="old_category" class="waves-effect">Old Producut</a> </li>
                <li><a href="#" value="highest_price" class="waves-effect">Highest Price</a> </li>
                <li><a href="#" value="lowest_price" class="waves-effect">Lowest Price</a> </li>
                <li><a href="#" value="" class="waves-effect">Clear Filter</a> </li>
              </ul>
              <!-- dropdown filter end -->
            </div>
          </div>
        </div>
        <div class="card-content" style="padding:0px;">
            <div id="result">

            </div>
            <div class="ajax">
              <?php echo $link; ?>
            </div>
        </div>
      </div>
    </div>
    <br><br>
    <!-- Manage Category section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php $this->load->view('Home/js.php');?>
    <!-- include js file include end -->
    <!-- custom js file include -->
    <script type="text/javascript">
      $('document').ready(function(){

        $('.ajax a').click(function(){
          var query = window.search;
          if(window.filter == null){
            var filter_cat = "";
          }else {
            var filter_cat = window.filter;
          }
          var page = $(this).attr('href');
          var parts = page.split("/");
          var url  = parts[parts.length-1];
          window.pagi = url;
          load_data(query,filter_cat,pagi);
        });

        $('#category_filter li a').click(function(e){
          event.preventDefault();
          var query  = window.search;
          var ul = window.pagi;
          window.filter = $(this).attr("value");
          load_data(query,filter,ul);
        });
        load_data();
        function load_data(query,filter,url=0)
        {
          alert(url);
          $.ajax({
            url:"<?php echo base_url();?>Admin/Ajax_Product_Search/"+ url ,
            method:"get",
            data:{query:query,filter:filter},
            success:function(data){
              $('#result').html(data);
            },
          });
        }
      $('#search_category').on('keyup mouseenter',function(e){
        event.preventDefault();
        if(window.filter == null){
          var filter_cat = "";
        }else {
          var filter_cat = window.filter;
        }
        var ul = window.pagi;
        window.search  = $(this).val();
        if(search!= " "){
          load_data(search,filter_cat,ul);
        }else {
          load_data();
        }
      });
      // Search With AJAX End
    });
    </script>
  </body>
</html>
