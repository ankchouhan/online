<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Delivered Orders - Online Shop</title>
    <!-- css file include start -->
    <?php $this->load->view('Home/css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <?php include('custom_css.php');?>
    <style media="screen">
      .btn-flat:hover{
        background: black;
        color:white;
      }
      #category_image{
        width:40px;
        height:40px;
        border-radius: 100%;
        border:1px solid silver;
      }
      .action_dropdown li a{
        color:gray;
        font-size:14px;
        font-weight:500;
      }
      #search_category{
        box-shadow: none;
        height:30px;
        padding-left: 10px;
        padding-right: 10px;
        border:none;
        padding-top:4px;
        line-height: 40px;
      }
      #category_filter {
        width: 13%!important;
        padding-top:8px;
        padding-bottom: 8px;
      }
      #category_filter li a{
        color:gray;
        font-size: 14px;
        font-weight:500;
      }
      table tr td{
        font-size:14px;
        padding:10px;
      }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- nav and side menu section start -->
    <?php include('nav.php');?>
    <!-- nav and side menu section end -->
    <!-- Manage Category section start -->
    <div class="container">
      <div class="card">
        <div class="card-content" style="border-bottom:1px solid silver;padding:8px;">
          <h5 style="font-weight:500;">Customers Details(<?= count($customer);?>)</h5>
        </div>
        <div class="card-content" style="padding-top:0px;">
          <table class="striped">
            <tr>
              <th>ID</th>
              <th>Username</th>
              <th>Email</th>
              <th>Mobile</th>
              <th>Password</th>
              <th>Address</th>
              <th>Register Date</th>
            </tr>
            <?php if(count($customer)):?>
              <?php foreach($customer as $cust):?>
            <tr>
              <td><?= $cust->id ;?></td>
              <td><?= $cust->fullname ;?></td>
              <td><?= $cust->email ;?></td>
              <td><?= $cust->mobile ;?></td>
              <td><?= $cust->password ;?></td>
              <td><?= $cust->address ;?></td>
              <td><?= $cust->register_date ;?></td>
            </tr>
          <?php endforeach;
            else:?>
          <?php endif;?>
          </table>
        </div>
      </div>
    </div>
    <!-- Manage Category section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php $this->load->view('Home/js.php');?>
    <!-- include js file include end -->
    <!-- custom js file include -->
  </body>
</html>
