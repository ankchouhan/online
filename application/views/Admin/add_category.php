<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Add Category - Online Shop</title>
    <!-- css file include start -->
    <?php $this->load->view('Home/css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <?php include('custom_css.php');?>
    <style media="screen">
      #input_box{
        border:1px solid silver;
        box-shadow: none;
        box-sizing: border-box;
        padding-left: 5px;
        padding-right: 5px;
        border-radius: 3px;
        height: 40px;
      }
      #input_file{
        border: 1px solid silver;
        padding: 8px;
        width:100%;
        margin-bottom: 15px;
        font-size:14px;
      }
      #category_image{
        width:50px;
        height:50px;
        border-radius: 100%;
        border:1px solid silver;
      }
    </style>
  </head>
  <body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">
    <!-- body section start -->
    <!-- nav and side menu section start -->
    <?php include('nav.php');?>
    <!-- nav and side menu section end -->
    <!-- category section start -->
    <div class="row" style="margin-top:10px;">
      <div class="col l5 m5 s12">
        <?= form_open_multipart('Admin/Upload_Category');?>
        <!-- add category section start -->
        <div class="card">
          <div class="card-content" style="border:1px solid silver;padding:10px;">
            <h6 style="font-size:15px;font-weight:500;">Add Category</h6>
          </div>
          <div class="card-content">
            <h6 style="font-size:14px;font-weight:500;margin-top:0px;">Category Name</h6>
            <input type="text" name="category_name" id="input_box" placeholder="Enter Category Name" required>
            <h6 style="font-size:14px;font-weight:500;">Image</h6>
            <input type="file" name="category_image" id="input_file" style="margin-bottom:0px;"required>
            <small style="color:red;">Max. Image Size : 2MB | 100px X 100px</small>
            <button type="submit" class="btn waves-effect waves-light" style="background:black;margin-top:15px;font-weight:500;display:block;text-transform:capitalize;">Save Category</button>
          </div>
        </div>
        <!-- add category section end -->
        <?= form_close();?>
      </div>
      <div class="col l7 m7 s12">
        <!-- recent category section Start -->
        <div class="card">
          <div class="card-content" style="border:1px solid silver;padding:10px;">
            <h6 style="font-size:15px;font-weight:500;"> Recent Upload Laptop Brands <span style="color:red;font-size:14px;">(Last 7 Days)</span> </h6>
          </div>
          <div class="card-content">
            <table class="table">
              <tr>
                <th class="center">ID</th>
                <th style="padding-left:30px;">Category Name</th>
                <th>Status</th>
                <th>Action</th>
              </tr >
              <?php if(count($categories)):?>
                <?php foreach($categories as $cat):?>
              <tr>
                <td>
                  <center>
                    <img src="<?= base_url().'uploads/category_image/'.$cat->cat_image;?>" alt="" class="responsive-img" id="category_image">
                  </center>
                </td>
                <td style="padding-left:30px;"><?= $cat->category_name;?></td>
                <td><?= ($cat->status == "0"? '<span class="fa fa-toggle-on" style="color:green;font-size:13px;"></span> Active':'<span class="fa fa-toggle-off" style="color:red;font-size:13px;"></span> Inactive');?></td>
                <td><a href="<?= base_url('Admin/Edit_Category/'.$cat->id);?>" style="color:gray;" title="Edit"><span class="fas fa-edit"></span></a> -
                  <a href="<?= base_url('Admin/Delete_Category/'.$cat->id.'/1');?>"onclick="return confirm('Are you sure to Delete this Category.')" style="color:gray;" title="Delete"> <span class="fa fa-trash-alt"></span> </a> </td>
              </tr>
            <?php endforeach;
              else :?>
              <td colspan="3" style="color:gray;text-align:center;font-weight:bold;">Category Not Found</td>
            <?php endif;?>
            </table>
          </div>
        </div>
        <!-- recent category section end -->
      </div>
    </div>
    <!-- category section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php $this->load->view('Home/js.php');?>
    <!-- include js file include end -->
    <!-- custom js file include -->
    <script>
    window.history.forward();
        function noBack() {
             window.history.forward();
        }
    </script>
  </body>
</html>
