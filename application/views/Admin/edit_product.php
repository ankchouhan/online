<?php
  if(count($products)){

  }else {
    $this->session->set_flashdata('error','This Product ID Not Found');
    return redirect('Admin/Manage_Product');
  }
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Edit Products - Online Shop</title>
    <!-- css file include start -->
    <?php $this->load->view('Home/css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <?php include('custom_css.php');?>
    <style media="screen">
      .container{
      width:55%;
      }
      #input_box{
        border:1px solid silver;
        box-shadow: none;
        box-sizing: border-box;
        padding-left: 5px;
        padding-right: 5px;
        border-radius: 3px;
        height: 40px;
        display: block;
      }
      select{
        display: block;
        border: 1px solid silver;
        outline:none;
        width:40%;
        height: 40px;
        border-radius: 3px;
      }
      textarea{
        border:1px solid silver;
        outline: none;
        resize: none;
        padding: 10px;
        border-radius: 3px;
        height:90px;
        width:40%;
      }
      #input_file{
        border-radius: 3px;
        border:1px solid silver;
        padding: 8px;
        width:40%;
        margin-top:5px;
      }
    </style>
  </head>
  <body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">
    <!-- body section start -->
    <!-- nav and side menu section start -->
    <?php include('nav.php');?>
    <!-- nav and side menu section end -->
    <!-- upload product section start -->
    <div class="container">
      <div class="card">
        <div class="card-content" style="border:1px solid silver;padding:10px;">
          <h6 style="font-size:18px;font-weight:700;">Edit Product</h6>
        </div>
        <div class="card-content">
          <?= form_open_multipart('Admin/Update_Product/'.$products[0]->id);?>
          <h6 style="font-size:14px;font-weight:500;margin-top:0px;">Product Title</h6>
          <input type="text" name="product_title" id="input_box" value="<?= $products[0]->product_title;?>" style="width:70%;" placeholder="Enter Product Title" required>
          <h6 style="font-size:14px;font-weight:500;">Product Image</h6>
          <img src="<?= base_url().'uploads/product_image/'.$products[0]->image;?>" alt="" class="responsive-img" style="width:100px;height:100px;border:2px dashed silver;display:block;">
          <input type="file" name="product_image" id="input_file">
          <h6 style="font-size:14px;font-weight:500;">Category</h6>
          <select  name="category_id" required>
            <option value="" selected>Select Category</option>
            <?php if(count($categories)):?>
              <?php foreach($categories as $cat):?>
                <?php if($products[0]->category_id == $cat->id ):?>
                  <option value ="<?= $cat->id ;?>" selected><?= $cat->category_name ;?></option>
                <?php else:?>
                  <option value ="<?= $cat->id ;?>"><?= $cat->category_name ;?></option>
                <?php endif;?>
          <?php endforeach;
            else:?>
            <option>Category Not found.</option>
          <?php endif;?>
          </select>
          <h6 style="font-size:14px;font-weight:500;">Short Description</h6>
          <textarea name="short_desc" placeholder="Enter Product Description"required><?= $products[0]->product_title;?></textarea>
          <h6 style="font-size:14px;font-weight:500;">Color</h6>
          <input type="text" name="color" id="input_box" value="<?= $products[0]->color;?>" style="width:40%;"placeholder="Enter Product Color" >
          <h6 style="font-size:14px;font-weight:500;">Weight</h6>
          <input type="text" name="weight" id="input_box" style="width:40%;" value="<?= $products[0]->weight;?>" placeholder="Enter Product Weight">
          <h6 style="font-size:14px;font-weight:500;">Price</h6>
          <input type="number" name="price" id="input_box" style="width:40%;" placeholder="Rs. 150" value= "<?= $products[0]->price;?>" required>
          <button type="submit" style="background:black;margin-top:15px;font-weight:500;text-transform:capitalize;" class="btn waves-effect waves-light" name="button">Update Product</button>
          <button type="Reset" style="background:red;color:white;margin-top:15px;font-weight:500;text-transform:capitalize;" class="btn waves-effect waves-light" name="button">Reset</button>
          <?= form_close();?>
        </div>
      </div>
    </div>
    <!-- upload product section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php $this->load->view('Home/js.php');?>
    <!-- include js file include end -->
    <!-- custom js file include -->
    <script type="text/javascript">
          window.history.forward();
              function noBack() {
                   window.history.forward();
              }
    </script>
  </body>
</html>
