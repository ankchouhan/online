<?php $this->load->helper('product'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Order Details - Online Shop</title>
    <!-- css file include start -->
    <?php $this->load->view('Home/css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <?php include('custom_css.php');?>
    <style media="screen">
    table tr td{
      font-size:14px;
      color:gray;
      font-weight:500;
    }
    select{
      display:block;
      padding:5px;
      border:1px solid silver;
      height:40px;
      margin-bottom: 10px;
    }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- nav and side menu section start -->
    <?php include('nav.php');?>
    <!-- nav and side menu section end -->
    <!-- Manage Category section start -->
    <div class="container">
      <div class="card">
        <div class="card-content" style="padding:8px;">
          <h5 style="margin-top:5px;font-size:20px;font-weight:500;">Shipping Address</h5>
          <h6 style="margin-top:5px;margin-left:10px;font-size:14px;font-weight:500;color:gray;">Username:&nbsp;<?= $order[0]->user_name;?></h6>
          <h6 style="margin-top:5px;margin-left:10px;font-size:14px;font-weight:500;color:gray;">Address:&nbsp;<?= $order[0]->shipping_address;?></h6>
        </div>
      </div>
      <div class="card">
        <div class="card-content" style="border-bottom:1px solid silver;padding:8px;">
          <h5 style="margin-top:5px;font-size:20px;font-weight:500;">Product List</h5>
        </div>
        <div class="card-content" style="padding:0px;">
          <table class="table striped">
            <tr>
              <th style="padding-left:20px;">Order ID</th>
              <th style="padding-left:30px;">Product Name</th>
              <th>Quantity</th>
              <th>Rate</th>
              <th style="padding-right:10px;">Total</th>
            </tr>
          <?php if(count($order_product)):?>
            <?php foreach($order_product as $ord):?>
            <tr>
              <td style="padding:20px;"><?= $ord->order_id;?></td>
              <td style="padding-left:20px;"> <a target="_blank" href="<?= base_url('Home/Product_Details/'.$ord->product_id)?>" style="color:gray;"><?= $ord->product_name;?></a></td>
              <td><?= $ord->quantity;?></td>
              <td><?= number_format($ord->rate);?></td>
              <td style="padding-right:10px;"><?php $total="";
              $total = ($ord->rate * $ord->quantity);
              echo number_format($total);?></td>
            </tr>
          <?php endforeach;
              else:?>
              <tr>
                <td colspan="5" style="color:gray;text-align:center;font-size:14px;font-weight:500;">Order Not Found</td>
              </tr>
            <?php endif;?>
          </table>
        </div>
      </div>
      <div class="card">
        <div class="card-content" style="padding:5px;">
          <?php if($uri != "1"):?>
          <?= form_open('Admin/Change_Order_Status/'.$order[0]->id);?>
          <h5 style="margin-top:5px;margin-bottom: 20px;font-size:20px;font-weight:500;">Update Status</h5>
          <select  name="status">
            <?php if($order[0]->order_status == "Pending"):?>
              <option selected>Pending</option>
              <option>Packed</option>
              <option>Dispatch</option>
              <option>Delivered</option>
            <?php elseif($order[0]->order_status == "Packed"):?>
              <option>Pending</option>
              <option selected>Packed</option>
              <option>Dispatch</option>
              <option>Delivered</option>
            <?php elseif($order[0]->order_status == "Dispatch"):?>
              <option>Pending</option>
              <option>Packed</option>
              <option selected>Dispatch</option>
              <option>Delivered</option>
            <?php elseif($order[0]->order_status == "Delivered"):?>
              <option>Pending</option>
              <option>Packed</option>
              <option>Dispatch</option>
              <option selected>Delivered</option>
            <?php else:?>
              <option selected>Select Status</option>
              <option>Pending</option>
              <option>Packed</option>
              <option>Dispatch</option>
              <option>Delivered</option>
            <?php endif;?>
          </select>
          <button type="submit" id="deliverd"name="button" class="btn waves-effect waves-light" style="background:black;margin-bottom:10px;text-transform:capitalize;">Update Status</button>
          <a href=<?= base_url('Admin/Print_Slip/'.$order[0]->id);?> target="_blank"; type="button" name="button" class="btn waves-effect waves-light" style="background:black;margin-bottom:10px;text-transform:capitalize;">Print Slip</a>
          <?= form_close();?>
        <?php else:?>
          <h6 style="color:gray;font-size:14px;font-weight:500;margin-left:10px;">This Product is Delivered <span class="fa fa-check-double"></span> </h6>
        <?php endif;?>
        </div>
      </div>
    </div>
    <!-- Manage Category section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php $this->load->view('Home/js.php');?>
    <!-- include js file include end -->
    <!-- custom js file include -->
    <script type="text/javascript">
      $(document).ready(function(){

        // $('#deliverd').click(function(){
        //   $('#deliverd').prop('disabled',true);
        // });

      });
    </script>

  </body>
</html>
