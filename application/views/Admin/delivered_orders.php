<?php $this->load->helper('product'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Delivered Orders - Online Shop</title>
    <!-- css file include start -->
    <?php $this->load->view('Home/css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <?php include('custom_css.php');?>
    <style media="screen">
      .btn-flat:hover{
        background: black;
        color:white;
      }
      #category_image{
        width:40px;
        height:40px;
        border-radius: 100%;
        border:1px solid silver;
      }
      .action_dropdown li a{
        color:gray;
        font-size:14px;
        font-weight:500;
      }
      #search_category{
        box-shadow: none;
        height:30px;
        padding-left: 10px;
        padding-right: 10px;
        border:none;
        padding-top:4px;
        line-height: 40px;
      }
      #category_filter {
        width: 13%!important;
        padding-top:8px;
        padding-bottom: 8px;
      }
      #category_filter li a{
        color:gray;
        font-size: 14px;
        font-weight:500;
      }
      table tr td{
        font-size:14px;
        padding:10px;
      }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- nav and side menu section start -->
    <?php include('nav.php');?>
    <!-- nav and side menu section end -->
    <!-- Manage Category section start -->
    <div class="container">
      <div class="card">
        <div class="card-content" style="border-bottom:1px solid silver;padding:8px;" >
          <h5 style="font-weight:500;">Delivered Orders</h5>
          <div class="row" style="margin-bottom:0px;">
            <div class="col l6 m6 s12">
              <ul>
                <li style="display:flex;border:1px solid silver;border-radius:60px;width:80%;" >
                  <input type="text" name="search_category" id="search_category" autocomplete="off" placeholder="Enter Order ID" required><span class="fa fa-search" style="background:none;border:none;margin-right:12px;margin-top:13px;"></span></input>
                </li>
                <li>
                  <!-- <h6 style="color:gray;font-size:14px;padding-left:10px;font-weight:500;">Note: Last 7 Days Delivered Product are not shown here.</h6> -->
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="card-content" style="padding:0px;">
            <div id="result">

            </div>
        </div>
      </div>
    </div>
    <!-- Manage Category section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php $this->load->view('Home/js.php');?>
    <!-- include js file include end -->
    <!-- custom js file include -->
    <script type="text/javascript">
      $('document').ready(function(){
        load_data();
        function load_data(query)
        {
          $.ajax({
            url:"<?php echo base_url();?>Admin/Ajax_Delivered_Order",
            method:"post",
            data:{query:query},
            success:function(data){
              $('#result').html(data);
            }
          });
        }
      $('#search_category').on('keyup mouseenter',function(){
        event.preventDefault();
        window.search  = $(this).val();
        if(search!= " "){
          load_data(search);
        }else {
          load_data();
        }
      });
      // Search With AJAX End
    });
    </script>
  </body>
</html>
