<!-- nav section start -->
<nav>
  <div class="nav-wrapper">
    <a href="<?= base_url('Admin/Dashboard');?>" class="brand-logo"><span class="fa fa-user-circle" style="margin-left:10px;"></span>&nbsp;Admin panel</a>
    <!-- right section start -->
    <ul class="right">
      <li><a href="#!" class="sidenav-trigger" data-target="side_menu" style="display:block;height:50px;line-height:50px;font-weight:500;">
        <span class="fa fa-bars"></span>&nbsp;Menu</a></li>
    </ul>
    <!-- right section end -->
  </div>
</nav>
<!-- nav section end -->
<!-- side menu section start -->
<ul class="sidenav collapsible" id="side_menu">
  <li><a href="<?=base_url('Admin/Dashboard');?>">Dashboard</a></li>
  <li>
    <div class="collapsible-header">Categories</div>
    <div class="collapsible-body">
        <ul>
          <li><a href="<?= base_url('Admin/Add_Category');?>">Add Categories</a></li>
          <li><a href="<?= base_url('Admin/Manage_Category');?>">Manage Categories</a></li>
        </ul>
    </div>
  </li>
  <li>
    <div class="collapsible-header">Products</div>
    <div class="collapsible-body">
        <ul id="body">
          <li><a href="<?= base_url('Admin/Add_Product');?>">Add Products</a></li>
          <li><a href="<?= base_url('Admin/Manage_Product');?>">Manage Products</a></li>
        </ul>
    </div>
  </li>
  <li>
    <div class="collapsible-header">Orders</div>
    <div class="collapsible-body">
        <ul>
          <li><a href="<?= base_url('Admin/Pending_Orders');?>">Pending Orders</a></li>
          <li><a href="<?= base_url('Admin/Delivered_Orders');?>">Delivered Orders</a></li>
          <li><a href="<?= base_url('Admin/Manage_Orders');?>">Manage All Orders</a></li>
        </ul>
    </div>
  </li>
  <li>
    <div class="collapsible-header">Sales</div>
    <div class="collapsible-body">
        <ul>
          <li><a href="<?= base_url('Admin/Today_Sales');?>">Today Sales</a></li>
          <li><a href="<?= base_url('Admin/All_Sales');?>"> All Time Sales</a></li>
        </ul>
    </div>
  </li>
  <li>
    <div class="collapsible-header">Cusotmers</div>
    <div class="collapsible-body">
        <ul>
          <li><a href="<?= base_url('Admin/Customers');?>">Manage Customers</a></li>
        </ul>
    </div>
  </li>
  <li><a href="<?= base_url('Admin/Signout');?>">Sign Out</a> </li>
</ul>
<!-- side menu section end -->
<?php if($msg = $this->session->userdata('success')):?>
<!-- message section start -->
  <div class="card">
    <div class="card-content" style="padding:8px;padding-left:15px;">
      <h6 style="font-size:14px;font-weight:500;color:green;margin-top:5px;"><span class="fas fa-check-double"></span>&nbsp;<?= $msg;?></h6>
    </div>
  </div>
<!-- message section start -->
<?php endif;?>
<?php if($msg = $this->session->userdata('error')):?>
<!-- message section start -->
  <div class="card">
    <div class="card-content" style="padding:8px;padding-left:15px;">
      <h6 style="font-size:14px;font-weight:500;margin-top:5px;color:red;"><span class="fas fa-exclamation-triangle"></span>&nbsp;<?= $msg;?></h6>
    </div>
  </div>
<!-- message section start -->
<?php endif;?>
