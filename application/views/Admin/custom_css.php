<style media="screen">
#order_dropdown,#seller_dropdown,#product_dropdown,#income_dropdown{
  width:50%!important;
  padding-top:8px;
  padding-bottom: 8px;
}
#order_dropdown li a,#seller_dropdown li a,#product_dropdown li a,#income_dropdown li a{
  color: gray;
  font-size: 14px;
  font-weight:500;
}
#top_sold_products li{
  border-bottom: 1px dashed silver;
  padding:10px;
}
#top_sold_products li:hover{
  background: rgba(0,0,0,0.1);
}
nav{
  height:50px;
  line-height:50px;
}
nav .brand-logo{
  font-size:20px;
  font-weight:500;
}
.collapsible-header{
  padding-left:30px!important;
  font-size: 14px;
  font-weight:500;
}
.collapsible-header:hover{
  background: black!important;
  color:white;
  width: 100%!important;
}
#side_menu li a:hover{
  background: black;
  color:white;
}
.collapsible-body ul li a{
  padding-left:40px!important;

}
</style>
