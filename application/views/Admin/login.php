<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Admin Login - Online Shop</title>
    <!-- css file include start -->
    <?php $this->load->view('Home/css.php');?>
    <style media="screen">
    body {
      margin: 0;
      padding: 0;
      /* font-family: sans-serif; */
      background: linear-gradient(to right, #b92b27, #1565c0);
    }
    .spinner-wrapper {
      position:fixed;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      z-index: 999999;
      backdrop-filter:blur(8px);
    }
    .spinner{
      position: absolute;
      top: 42%;
      left: 42%;
    }
    .box {
      width: 500px;
      padding: 40px;
      position: absolute;
      top: 50%;
      left: 25%;
      background: #191919;
      text-align: center;
      transition: 0.25s;
      margin-top: 100px;
    }
    .box input[type="text"],
    .box input[type="password"]{
      border: 0;
      background: none;
      display: block;
      margin: 20px auto;
      text-align: center;
      border: 2px solid #3498db;
      padding: 10px 10px;
      width: 250px;
      outline: none;
      color: white;
      border-radius: 24px;
      transition: 0.25s;
      height:22px;
    }
    .box h4 {
      color: white;
      font-weight:500;
    }
    .box input[type="text"]:focus,
    .box input[type="password"]:focus {
      width: 300px;
      border-color: #2ecc71;
    }

    .box input[type="submit"] {
      border: 0;
      background: none;
      display: block;
      margin: 20px auto;
      text-align: center;
      border: 2px solid #2ecc71;
      padding: 14px 40px;
      outline: none;
      color: white;
      border-radius: 24px;
      transition: 0.25s;
      cursor: pointer;
    }

    .box input[type="submit"]:hover {
      background: #2ecc71;
    }

    .forgot {
      text-decoration: underline;
    }

    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- login form section start -->
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="card">
            <div class="box">
              <h4 style="color:#7619db;margin-bottom:0px;"><span class="fa fa-users"></span></h4>
              <h4 style="margin-top:0px;">Admin Login</h4>
                  <input type="text" name="username" id="admin_username"  placeholder="Username" autocomplete="off">
                  <input type="password" name="password" id="admin_password" placeholder="Password">
                  <a class="forgot text-muted" href="#">Forgot password?</a>
                  <input type="submit" name="" id="btn_login" href="#!" value="Login">
                </div>
              </div>
            </div>
          </div>
        </div>
    <!-- login form section end -->
    <!-- preloader section start -->
    <div id="preloader" style="display:none;">
      <div class="spinner-wrapper">
        <div class="spinner">
          <img src="<?= base_url('assets/image/731.gif')?>" alt="">
        </div>
      </div>
    </div>
    <!-- preloader section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php $this->load->view('Home/js.php');?>
    <!-- include js file include end -->
    <!-- custom js file include -->
    <script type="text/javascript">
      $('document').ready(function(){
        // admin login script start
        $('#btn_login').click(function(){
          var username = $('#admin_username').val();
          var password = $('#admin_password').val();
          if(username == "")
          {
            M.toast({html:"Please Enter Username"});
          }else if (password == "") {
            M.toast({html:"Please Enter Password"});
          }
          else {
            $.ajax({
              type:'ajax',
              method:'POST',
              url:'<?=base_url('Admin/Login');?>',
              data:{username:username,password:password},
              beforeSend:function(data){
                $('#preloader').show();
              },
              success:function(data){
                $('#preloader').hide();
                if(data == 1){
                  window.location.href ='<?= base_url('Admin/Dashboard');?>';
                }else {
                  M.toast({html:'Your Username & Password is Incorrect'});

                }
              },
              error:function(){
                alert('Error ! Admin Login Account');
              }
            });
          }
        });
        // admin login script end
      });
    </script>
  </body>
</html>
