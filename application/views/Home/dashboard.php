<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>User SignUp - Online Shop</title>
    <!-- css file include start -->
    <?php include('css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <!-- custom css file -->
    <style>
    .btn-flat:hover{
      background:#7a745c;
      color: white;
    }
    #category_filter{
      padding-top: 10px;
      padding-bottom: 10px;
    }
    #category_filter li a{
      color:gray;
      font-size: 14px;
    }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- top & menu bar File start -->
    <?php include('top_menu_bar.php');?>
    <!-- top & menu bar File end-->
    <!-- dashboard section start -->
    <h4 style="padding-left:10px;font-size:25px;font-weight:500;">Welcome To <?= $user[0]->fullname;?></h4>
    <div class="row">
      <div class="col l3 m4 s12">
        <!-- card section start -->
        <div class="card">
          <div class="card-content">
            <div class="row" style="margin-bottom:0px;">
              <div class="col l4 m4 s4">
                <h4 style="margin-top:0px;"><span class="fa fa-shopping-cart" style="color:gray;font-size:70px;"></span></h4>
              </div>
              <div class="col l8 m8 s8">
                <h6 class="right-align" style="color:gray;font-size:15px;margin-top:0px;">My Cart</h6>
                <h4 class="right-align" style="margin-top:0px;font-weight:500;"><?= count($carts);?></h4>
              </div>
            </div>
          </div>
          <div class="card-action">
            <a href="<?= base_url('Home/Carts');?>" style="color:black;"> <b>View Cart</b></a>
          </div>
        </div>
        <!-- card section end -->
      </div>
      <div class="col l3 m4 s12">
        <!-- card section start -->
        <div class="card">
          <div class="card-content">
            <div class="row" style="margin-bottom:0px;">
              <div class="col l4 m4 s4">
                <h4 style="margin-top:0px;"><span class="fa fa-shopping-basket" style="color:gray;font-size:70px;"></span></h4>
              </div>
              <div class="col l8 m8 s8">
                <h6 class="right-align" style="color:gray;font-size:15px;margin-top:0px;">My Order's</h6>
                <h4 class="right-align" style="margin-top:0px;font-weight:500;"><?= count($orders)?></h4>
              </div>
            </div>
          </div>
          <div class="card-action">
            <a href="<?= base_url('Home/My_Order');?>" style="color:black;"> <b> View Order's</b></a>
          </div>
        </div>
        <!-- card section end -->
      </div>
      <div class="col l3 m4 s12">
        <!-- card section start -->
        <div class="card">
          <div class="card-content">
            <div class="row" style="margin-bottom:0px;">
              <div class="col l4 m4 s4">
                <h4 style="margin-top:0px;"><span class="fa fa-shipping-fast" style="color:gray;font-size:70px;"></span></h4>
              </div>
              <div class="col l8 m8 s8">
                <h6 class="right-align" style="color:gray;font-size:15px;margin-top:0px;">Delivered Order's</h6>
                <h4 class="right-align" style="margin-top:0px;font-weight:500;"><?= count($delivered);?></h4>
              </div>
            </div>
          </div>
          <div class="card-action">
            <a href="<?= base_url('Home/Delivered_Orders');?>" style="color:black;"><b>View Delivered Orders</b></a>
          </div>
        </div>
        <!-- card section end -->
      </div>
      <div class="col l3 m4 s12">
        <!-- card section start -->
        <div class="card">
          <div class="card-content">
            <div class="row" style="margin-bottom:0px;">
              <div class="col l4 m4 s4">
                <h4 style="margin-top:0px;"><span class="fa fa-user-cog" style="color:gray;font-size:70px;"></span></h4>
              </div>
              <div class="col l8 m8 s8">
                <h6 class="right-align" style="color:gray;font-size:15px;margin-top:0px;">My Profile</h6>
                <h4 class="right-align" style="margin-top:0px;font-size:25px;">Manage Profile</h4>
              </div>
            </div>
          </div>
          <div class="card-action">
            <a href="<?= base_url('Home/Manage_Profile');?>" style="color:black;"> <b>Manage Profile</b></a>
          </div>
        </div>
        <!-- card section end -->
      </div>
    </div>
    <div class="container">
      <p style="text-align:center;font-size:14px;color:gray;font-weight:500;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
        . Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
    </div>
    <!-- dashboard section end -->
    <!-- footer section start -->
    <?php $this->load->view('Home/footer.php');?>
    <!-- footer section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php include('js.php');?>
    <!-- include js file include end -->
  </body>
</html>
