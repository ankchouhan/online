<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>My Order's(<?= count($orders); ?>) - Online Shop</title>
    <!-- css file include start -->
    <?php include('css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <!-- custom css file -->
    <style>
    .btn-flat:hover{
      background:#7a745c;
      color: white;
    }
    #category_filter{
      padding-top: 10px;
      padding-bottom: 10px;
    }
    #category_filter li a{
      color:gray;
      font-size: 14px;
    }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- top & menu bar File start -->
    <?php include('top_menu_bar.php');?>
    <!-- top & menu bar File end-->
    <!-- my order section start -->
    <h4 style="padding-left:10px;font-size:25px;font-weight:500;">My Orders(<?= count($orders); ?>)</h4>
    <div class="container">
      <?php if(count($orders)):?>
        <?php foreach($orders as $order):?>
        <!-- card section start -->
        <div class="card">
          <div class="card-content" style="border-bottom:1px solid silver;padding:10px;">
            <a href="#" class="btn waves-effect waves-light" style="background:black;box-shadow:none;">Order ID-<?= $order->id;?></a>
            <a href="#" class="btn btn-flat waves-effect waves-light right" style="background:none;color:gray;border:1px solid silver;
            border-radius:2px;"> <span class="fa fa-map-marker-alt"></span> Track Order</a>
          </div>
          <div class="card-content" style="border-bottom:1px solid silver;padding:10px;">
            <?php $this->load->helper('product');
              $product = get_product_by_order_id($order->id);
                ?>
            <?php if(count($product)):?>
              <?php foreach ($product as $pro):
                $image = get_product_details($pro->product_id);?>
            <div class="row" style="margin-bottom:0px;padding-top:0px;border-bottom:1px dashed silver;">
              <div class="col l2 m3 s12" style="margin-top:5px;margin-bottom:0px;">
                <img src="<?= base_url().'uploads/product_image/'.$image[0]->image;?>" style="width:100px;height:100px;"alt="" class="responsive-img">
              </div>
              <div class="col l5 m5 s12">
                <h5 style="font-size:20px;font-weight:500;"><?= $pro->product_name;?></h5>
                <h6 style="font-size:14px;color:gray;margin-top:0px;">Quantity : <?= $pro->quantity;?></h6>
              </div>
              <div class="col l5 m4 s12">
                <h5 style="font-size:20px;font-weight:500;"> <span class="fa fa-rupee-sign"></span>&nbsp;<?= number_format($pro->rate);?> </h5>
                <h6 style="font-size:14px;color:gray;margin-top:0px;">
                  <?php if($order->order_status == "Delivered"):
                    $status ="Delivered <span class='fa fa-people-carry'></span>";
                  elseif($order->order_status == "Dispatch"):
                    $status = "Dispatch <span class='fa fa-shipping-fast'></span>";
                    elseif($order->order_status == "Packed"):
                      $status = "Packed <span class='fa fa-box'></span>";
                      else:
                        $status = "Pending <span class='fas fa-clock'></span>";?>
                  <?php endif;?>
                  Your product is <?= $status;?></h6>
              </div>
            </div>
          <?php endforeach;?>
            <?php else:?>
          <?php endif;?>
          </div>
          <div class="card-content" style="padding:10px;">
            <h6 style="margin-top:5px;">Orderd On : <b><?= date('D, M. d Y',strtotime($order->order_date));?></b> <span class=" right" style="margin-right:10px;">Order Total : <b><span class="fa fa-rupee-sign"></span>&nbsp;<?= number_format($order->total_amount);?></span></b> </h6>
          </div>
        </div>
        <?php endforeach;
          else:?>
          <h6 style="color:gray;font-weight:500;text-align:center;">Order's Not Found</h6>
        <?php endif;?>
        <!-- card section end -->
    </div>
    <!-- my order section end -->
    <!-- footer section start -->
    <?php $this->load->view('Home/footer.php');?>
    <!-- footer section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php include('js.php');?>
    <!-- include js file include end -->
  </body>
</html>
