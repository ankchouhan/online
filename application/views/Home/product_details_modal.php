    <?php $this->load->helper('product');?>
    <!-- Product modal section -->
      <!-- <div class="card" style="margin-top:10px;"> -->
      <span class="right modal-close" style="padding:8px 12px;background:red;color:white;margin-right:25px;"><b>X</b> </span>
        <div class="card-content" style="padding:5px;border-bottom:1px solid silver;">
          <div class="row" style="border:1px solid gray;">
            <div class="col l5 m5 s12" style="margin-top:20px;">
              <img src="<?= base_url().'uploads/product_image/'.$product[0]->image;?>" alt="" class="responsive-img" style="width:100%;height:430px;border:1px solid rgba(0,0,0,0.1);">
            </div>
            <div class="col l6 m6 s12" style="margin-top:20px;">
              <h6 style="margin-top:0px;font-weight:500;font-size:20px;"><?= $product[0]->product_title;?></h6>
              <?php
              $category_details = get_category_details($product[0]->category_id);
              ?>
              <h6 style="font-size:14px;color:silver"><a style="color:silver;" href="<?= base_url('Home/index');?>">Home</a> <span class="fa fa-angle-double-right"></span>
              <a href="<?= base_url('Home/Product_Categories/'.$product[0]->category_id);?>" style="color:silver;"><?= $category_details[0]->category_name; ?></a>
              <span class="fa fa-angle-double-right"></span> <a href="<?= base_url('Home/Product_Details/'.$product[0]->id);?>" style="color:black;"><?= word_limiter($product[0]->product_title,4);?></a></h6>
              <div class="divider" style="margin-top:15px;margin-bottom:15px;"></div>
              <p style="font-size:14px;color:gray;line-height:20px;"><?= $product[0]->short_desc;?><br>
                <h6 style="font-size:15px;font-weight:500;color:gray;">Color:&nbsp;<?= $product[0]->color;?></h6>
                <h6 style="font-size:15px;font-weight:500;color:gray;margin-top:0px;">Weight:&nbsp;<?= $product[0]->weight;?></h6>
                <div class="divider" style="margin-top:15px;margin-bottom:15px;"></div>
                <div class="row">
                <div class="col l2 m2 s12">
                  <img src="<?= base_url('assets/image/home_image/contact.png')?>" alt="" class="responsive-img" style="width:50px;height:50px;">
                  <h6 style="font-size:12px;font-weight:500;color:gray;">No-Contact <br> Delivery</h6>
                </div>
                <div class="col l2 m2 s12">
                  <img src="<?= base_url('assets/image/home_image/deliver.png')?>" alt="" class="responsive-img"  style="width:50px;height:50px;">
                  <h6 style="font-size:12px;font-weight:500;color:gray;">10-Days <br>Returnable</h6>
                </div>
                <div class="col l2 m2 s12">
                  <img src="<?= base_url('assets/image/home_image/amazon.png')?>" alt="" class="responsive-img"  style="width:50px;height:50px;">
                  <h6 style="font-size:12px;font-weight:500;color:gray;">Fast<br>Delivered</h6>
                </div>
                <div class="col l2 m2 s12">
                  <img src="<?= base_url('assets/image/home_image/warrenty.png')?>" alt="" class="responsive-img"  style="width:50px;height:50px;">
                  <h6 style="font-size:12px;font-weight:500;color:gray;">6 Month <br>Warrenty</h6>
                </div>
              </div>
                <h5> <b> <span class="fa fa-rupee-sign">&nbsp;<?= number_format($product[0]->price);?></span> </b> </h5>
                <div class="row" style="margin-top:20px;">
                  <div class="col l6 m6 s12">
                  <button type="button" onclick="add_to_cart('<?= $product[0]->id;?>')" name="button" class="btn waves-effect" style="box-shadow: none;text-transform:capitalize;background:#d9d5c5;font-weight:500;color:black;width:100%;">
                  <span class="fa fa-shopping-cart"></span>&nbsp;Add To Cart</button>
                  </div>
                  <div class="col l6 m6 s12">
                  <a href="<?= base_url('Home/Buy_Product/'.$product[0]->id);?>" type="button" name="button" class="btn waves-effect waves-light" style="box-shadow: none;text-transform:capitalize;background:black;width:100%;">
                    <span class="fa fa-cube"></span>&nbsp;Buy Now</a>
                  </div>
                </div>
              </p>
            </div>
          </div>
        </div>
      </div>


    <!-- Product modal section -->
