<?php $this->load->helper('product'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Order Slip - Online Shop</title>
    <!-- css file include start -->
    <?php $this->load->view('Home/css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <?php include('custom_css.php');?>
    <style media="screen">
    </style>
  </head>
  <body style="padding:15px;background:white;" onload="window.print();">
    <!-- body section start -->
    <h4 style="font-weight:500;"><span class="fab fa-shopware"></span>nline Shop</h4>
    <table >
      <tr>
        <td>
          <h5 style="font-weight:500;font-size:20px;">Ship To</h5>
          <h6 style="color:gray;font-weight:500;font-size:15px;"><?= $order[0]->user_name;?>,</h6>
          <h6 style="color:gray;font-weight:500;font-size:15px;margin-top:0px;"><?= $order[0]->shipping_address;?></h6>
          <h6 style="color:gray;font-weight:500;font-size:15px;margin-top:0px;">Order Date:&nbsp;<?= date("d M, Y",strtotime($order[0]->order_date));?></h6>
        </td>
        <td class="right-align" style="padding-right:25px;">
          <h6 style="font-weight:500;font-size:20px;padding-left:90px;">Online Shop</h6>
          <h6 style="color:gray;font-weight:500;font-size:15px;margin-top:0px;line-height:20px;">12,Goutam-Marg,Ujjain-456006 <br> Madhay-Pradesh India</h6>
        </td>
      </tr>
    </table>
    <table>
      <tr>
        <th>Order ID</th>
        <th>Product Name</th>
        <th class="center-align">quantity</th>
        <th class="right-align">rate</th>
        <th class="right-align">Total</th>
      </tr>
      <?php
      $grand_total = "";
      if(count($order_product)):?>
        <?php foreach($order_product as $ord):
          $grand_total += ($ord->quantity * $ord->rate);?>
      <tr>
        <td style="font-size:15px;font-weight:500;">#<?= $ord->order_id;?></td>
        <td style="padding-right:20px;font-size:15px;font-weight:500;line-height:20px;width:50%;"><?= $ord->product_name?></td>
        <td class="center-align" style="font-size:15px;font-weight:500;"><?= $ord->quantity;?></td>
        <td class="right-align" style="font-size:15px;font-weight:500;"><span class="fa fa-rupee-sign"></span>&nbsp;<?= number_format($ord->rate);?></td>
        <?php $total = ($ord->quantity * $ord->rate);?>
        <td class="right-align"style="font-size:15px;font-weight:500;"><span class="fa fa-rupee-sign"></span>&nbsp; <?= number_format($total);?></td>
      </tr>
    <?php endforeach;
      else:
      $grand_total = "0";
      ?>
    <?php endif;?>
      <tr>
        <th colspan="4">Grand Total:</th>
        <th class="right-align"><span class="fa fa-rupee-sign"></span>&nbsp;<?= number_format($grand_total);?></th>
      </tr>
    </table>
    <table>
      <tr>
        <td>
          <h6 style="font-size:15px;font-weight:500;">Note : </h6>
          <p  style="font-size:14px;line-height:20px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
             Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </td>
      </tr>
      <tr>
        <td>
          <h6 tyle="font-size:15px;font-weight:500;">terms & Condition : </h6>
          <p  style="font-size:14px;line-height:20px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
             Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </td>
      </tr>
    </table>
    <!-- body section end -->
    <!-- include js file include start -->
    <?php $this->load->view('Home/js.php');?>
    <!-- include js file include end -->
    <!-- custom js file include -->

  </body>
</html>
