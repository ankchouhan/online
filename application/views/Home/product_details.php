<?php $this->load->helper('product'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title><?= (count($product))?$product[0]->product_title:"Product Not Found";?> - Online Shop</title>
    <!-- css file include start -->
    <?php include('css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <!-- custom css file -->
    <style>
    .btn-flat:hover{
      background:black;
      color: white;
    }
    #category_filter{
      padding-top: 10px;
      padding-bottom: 10px;
    }
    #category_filter li a{
      color:gray;
      font-size: 14px;
    }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- top & menu bar File start -->
    <?php include('top_menu_bar.php');?>
    <!-- top & menu bar File end-->
    <!-- card section start -->
      <div class="card" style="margin-top:10px;">
        <div class="card-content" style="padding:10px;border-bottom:1px solid silver;">
          <div class="row" style="border-bottom:1px solid silver;">
            <div class="col l4 m4 s12" style="margin-top:10px;">
              <img src="<?= base_url().'uploads/product_image/'.$product[0]->image;?>" alt="" class="responsive-img" style="width:100%;height:430px;border:1px solid rgba(0,0,0,0.1);">
            </div>
            <div class="col l5 m5 s12" style="margin-top:10px;">
              <h6 style="margin-top:0px;font-weight:500;font-size:20px;"><?= $product[0]->product_title;?></h6>
              <?php
              $category_details = get_category_details($product[0]->category_id);
              ?>
              <h6 style="font-size:14px;color:silver"><a style="color:silver;" href="<?= base_url('Home/index');?>">Home</a> <span class="fa fa-angle-double-right"></span>
              <a href="<?= base_url('Home/Product_Categories/'.$product[0]->category_id);?>" style="color:silver;"><?= $category_details[0]->category_name; ?></a>
              <span class="fa fa-angle-double-right"></span> <a href="<?= base_url('Home/Product_Details/'.$product[0]->id);?>" style="color:black;"><?= word_limiter($product[0]->product_title,4);?></a></h6>
              <div class="divider" style="margin-top:15px;margin-bottom:15px;"></div>
              <p style="font-size:14px;color:gray;line-height:20px;"><?= $product[0]->short_desc;?><br>
                <h6 style="font-size:15px;font-weight:500;color:gray;">Color:&nbsp;<?= $product[0]->color;?></h6>
                <h6 style="font-size:15px;font-weight:500;color:gray;margin-top:0px;">Weight:&nbsp;<?= $product[0]->weight;?></h6>
                <div class="divider" style="margin-top:15px;margin-bottom:15px;"></div>
                <div class="row">
                <div class="col l2 m2 s12">
                  <img src="<?= base_url('assets/image/home_image/contact.png')?>" alt="" class="responsive-img" style="width:50px;height:50px;">
                  <h6 style="font-size:12px;font-weight:500;color:gray;">No-Contact <br> Delivery</h6>
                </div>
                <div class="col l2 m2 s12">
                  <img src="<?= base_url('assets/image/home_image/deliver.png')?>" alt="" class="responsive-img"  style="width:50px;height:50px;">
                  <h6 style="font-size:12px;font-weight:500;color:gray;">10-Days <br>Returnable</h6>
                </div>
                <div class="col l2 m2 s12">
                  <img src="<?= base_url('assets/image/home_image/amazon.png')?>" alt="" class="responsive-img"  style="width:50px;height:50px;">
                  <h6 style="font-size:12px;font-weight:500;color:gray;">Fast<br>Delivered</h6>
                </div>
                <div class="col l2 m2 s12">
                  <img src="<?= base_url('assets/image/home_image/warrenty.png')?>" alt="" class="responsive-img"  style="width:50px;height:50px;">
                  <h6 style="font-size:12px;font-weight:500;color:gray;">6 Month <br>Warrenty</h6>
                </div>
              </div>
                <h5> <b> <span class="fa fa-rupee-sign">&nbsp;<?= number_format($product[0]->price);?></span> </b> </h5>
                <div class="row" style="margin-top:20px;">
                  <div class="col l6 m6 s12">
                  <button type="button" name="button" onclick="add_to_cart(<?=$product[0]->id;?>)" class="btn waves-effect" style="box-shadow: none;text-transform:capitalize;background:#d9d5c5;font-weight:500;color:black;width:100%;">
                  <span class="fa fa-shopping-cart"></span>&nbsp;Add To Cart</button>
                  </div>
                  <div class="col l6 m6 s12">
                  <a href="<?= base_url('Home/Buy_Product/'.$product[0]->id);?>"type="button" name="button" class="btn waves-effect waves-light" style="box-shadow: none;text-transform:capitalize;background:black;width:100%;">
                    <span class="fa fa-cube"></span>&nbsp;Buy Now</a>
                  </div>
                </div>
              </p>
            </div>
            <div class="col l3 m3 s12">
              <div class="card" style="box-shadow:none;border:1px dashed silver;">
                <div class="card-content" style="padding::10px;">
                  <h6 style="font-size:15px;font-weight:500;margin-top:0px;">Gift Card</h6>
                  <p style="color:gray;font-size:14px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit </p>
                </div>
              </div>
              <div class="card" style="box-shadow:none;border:1px dashed silver;">
                <div class="card-content" style="padding::10px;">
                  <h6 style="font-size:15px;font-weight:500;margin-top:0px;">Free Shipping</h6>
                  <p style="color:gray;font-size:14px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit </p>
                </div>
              </div>
              <div class="card" style="box-shadow:none;border:1px dashed silver;">
                <div class="card-content" style="padding::10px;">
                  <h6 style="font-size:15px;font-weight:500;margin-top:0px;">Money Back Gurantee</h6>
                  <p style="color:gray;font-size:14px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit </p>
                </div>
              </div>
              <div class="card" style="box-shadow:none;border:1px dashed silver;">
                <div class="card-content" style="padding::10px;">
                  <h6 style="font-size:15px;font-weight:500;margin-top:0px;">Online Support 24 X 7</h6>
                  <p style="color:gray;font-size:14px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit </p>
                </div>
              </div>
            </div>
          </div>
          <!-- releted product sectio start -->
          <?php
          $category_details = get_category_details($product[0]->category_id);
          ?>
          <h5 style="padding-left:10px;">Releated Products<span class="right"> <a href="<?= base_url('Home/Product_Categories/'.$category_details[0]->id);?>" class="btn waves-effect waves-light"
             style="text-transform:capitalize;background:black;margin-right:10px;">View More</a> </span></h5>
             <div class="row" style="margin-bottom:0px;margin-top:15px;">
               <?php if(count($releated_product)):?>
               <?php foreach($releated_product as $rp):?>
               <div class="col l2 m3 s6">
                 <!-- card section start -->
                 <a href="<?= base_url('Home/product_Details/'.$rp->id);?>">
                 <div class="card waves-effect">
                   <div class="card-image">
                     <img src="<?= base_url().'uploads/product_image/'.$rp->image;?>" alt="" style="width:100%;height:200px;padding:5px;">
                   </div>
                   <div class="card-content" style="border-Bottom:1px solid silver;padding:10px;">
                     <h6 style="font-size:15px;font-weight:500;margin-top:3px;" title="<?= $rp->product_title;?>"><?= word_limiter($rp->product_title,3);?></h6>
                     <h6 style="font-size:14px;color:gray;margin-top:5px;"><?php $category_data = get_category_details($rp->category_id);
                     echo $category_data[0]->category_name; ?></h6>
                     <h5 style="font-size:20px;color:green;font-weight:500;margin-top:0px;margin-bottom:3px;"> <span class="fa fa-rupee-sign"></span>&nbsp;<?= number_format($rp->price);?></h5>
                   </div>
                   <div class="card-content" style="padding:2px;">
                     <center>
                       <a  href="#!"  data-position="top" data-tooltip="Add To Cart" onclick="add_to_cart('<?= $rp->id;?>')"  class="btn btn-flat btn-floating waves-effect tooltipped"> <span class="fa fa-shopping-cart"></span> </a>
                       <a href="#!"  data-position="top" data-tooltip="View Product" onclick="product_detail_modal('<?= $rp->id;?>')" class="btn btn-flat btn-floating waves-effect tooltipped"> <span class="fa fa-eye"></span> </a>
                     </center>
                   </div>
                 </div>
                 </a>
                 <!-- card section end -->
               </div>
             <?php endforeach;
                else:?>
                <h6 colspan="5" style="margin-left: 5px;font-size: 14px;color:gray;font-weight:500;">Releated Product Not Found</h6>
              <?php endif;?>
             </div>
          <!-- releted product sectio ends -->
        </div>
      </div>
    <!-- card section end  -->
    <!-- footer section start -->
    <?php $this->load->view('Home/footer.php');?>
    <!-- footer section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php include('js.php');?>
    <!-- include js file include end -->
  </body>
</html>
