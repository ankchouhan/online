<?php $this->load->helper('product'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>My Cart(<?= count($carts);?>) - Online Shop</title>
    <!-- css file include start -->
    <?php include('css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <!-- custom css file -->
    <style>
    .btn-flat:hover{
        background:black;
        color:white;
    }
    #quantity_form{
      display: flex;
    }
    #quantity_form input[name="quantity"]{
    border: 1px solid silver;
    box-shadow: none;
    box-sizing: border-box;
    padding-left: 10px;
    padding-right: 10px;
    height:40px;
    border-radius: 3px;
    width:17%;
    }
    #quantity_form li{
      margin:3px;
    }
    .btn-floating:hover{
      background:black;
      border-radius: 100%;
      color:white;
    }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- top & menu bar File start -->
    <?php include('top_menu_bar.php');?>
    <!-- top & menu bar File end-->
    <!-- cart section start -->
    <div class="row" style="margin-bottom:0px;margin-top:10px;">
      <div class="col l8 m7 s12">
        <!-- card sectio start -->
        <div class="card">
          <div class="card-content" style="border-bottom:1px solid rgba(0,0,0,0.1);padding:10px;">
            <h5 style="font-size:20px;margin-top:5px;">My Carts(<?= count($carts); ?>)</h5>
          </div>
          <div class="card-content">
            <?php if(count($carts)):?>
              <?php foreach($carts as $cart):
              $product = get_product_details($cart->product_id);?>
            <div class="row" style="margin-bottom:0px;border-bottom:1px dashed silver;">
              <div class="col l3 m3 s12">
                <img src="<?= base_url().'uploads/product_image/'.$product[0]->image;?>" alt="" class="responsive-img" style="width:100px;height:100px;margin-left:27px;margin-top:5px;">
                <!-- quantity form section start -->
                <ul id="quantity_form" style="margin-top:0px;">
                  <li>
                    <button type="button" onclick="update_quantity('sub','<?= $cart->product_id;?>')" class="btn btn-floating btn-flat"style="box-shadow:none;font-weight:500;">-</button>
                  </li>
                  <input type="text" name="quantity" id="qty_<?= $cart->product_id;?>" value="<?= $cart->quantity;?>" readonly="" style="width:70px;text-align:center;margin-top:0px;">
                  <li>
                    <button type="button" onclick="update_quantity('add','<?= $cart->product_id;?>')" class="btn btn-floating btn-flat" style="box-shadow:none;font-weight:500;">+</button>
                  </li>
                </ul>
                <!-- quantity form section end -->
              </div>
              <div class="col l9 m9 s12" style="padding-left:0px;">
                <h6 style="font-size:15px;font-weight:500;"><?= $cart->product_name;?></h6>
                <h5 style="font-size:20px;font-weight:bold;margin-top:0px;"> <span class="fa fa-rupee-sign"></span>&nbsp;<?= number_format($cart->quantity * $cart->rate);?> </h5>
                <h6 style="font-size:14px;color:gray;"><?= $cart->quantity;?> Item X <?= number_format($cart->rate);?></h6>
                <a href="<?=base_url('Home/Product_Details/'.$cart->product_id);?>" target="_blank" class="btn btn-flat" style="text-transform:capitalize;margin-top:7px;">View Item</a>
                <a href="<?= base_url('Home/Remove_Product/'.$cart->id);?>" class="btn btn-flat" style="text-transform:capitalize;margin-top:7px;">Remove Item</a>
              </div>
            </div>
          <?php endforeach;
            else:?>
            <center>
              <h3><a href="<?= base_url('Home/index');?>" style="color:black;"> <span class="fa fa-shopping-cart"></span> </a></h3>
              <h6 style="font-size:14px;">Your Cart is Empty ? <a href="<?= base_url('Home/index');?>">Start Shopping Now</a> </h6>
            </center>
          <?php endif;?>
          </div>
        </div>
        <!-- card sectio end -->
      </div>
      <div class="col l4 m5 s12">
        <!-- card section start -->
        <div class="card">
          <div class="card-content" style="border-bottom:1px solid rgba(0,0,0,0.1);padding:10px;">
            <h5 style="font-size:20px;margin-top:5px;">Price Details</h5>
          </div>
          <div class="card-content">
            <h6 style="font-size:15px;margin-top:0px;border-bottom:1px dashed silver;padding-bottom:15px;">Price&nbsp;(<?= count($carts);?> Items) <span class="right"> <span class="fa fa-rupee"></span>&nbsp;  <?php
            if(count($carts)):
              $t_amount = "0";
              foreach($carts as $cart):
                $t_amount += ($cart->rate * $cart->quantity);
              endforeach;
            else:
              $t_amount = "0";
            endif;
            echo number_format($t_amount);?></span> </h6>
            <h5 style="font-size:20px;margin-top:0px;border-bottom:1px dashed silver;padding-bottom:15px;font-weight:500;">Total Amount<span class="right"> <span class="fa fa-rupee"></span>&nbsp;
              <?php
            if(count($carts)):
              $t_amount = "0";
              foreach($carts as $cart):
                $t_amount += ($cart->rate * $cart->quantity);
              endforeach;
            else:
              $t_amount = "0";
            endif;
            echo number_format($t_amount);?></span> </h5>
              <!-- button section start -->
              <div class="row" style="margin-top:18px;margin-bottom:0px;">
                <?php if(count($carts)>0):?>
                <div class="col l6 m6 s12">
                  <a href="<?= base_url('Home/index');?>" class="btn waves-effect waves-light" style="font-size:12px;text-transform:capitalize;font-weight:700;width:100%;background:#d9d5c5;color:black;box-shadow:none;">Continue Shopping</a>
                </div>
                <div class="col l6 m6 s12">
                  <a href="<?= base_url('Home/Place_Order');?>" class="btn waves-effect waves-light" style="font-size:12px;text-transform:capitalize;font-weight:500;width:100%;background:black;">Place Order</a>
                </div>
              <?php else:?>
                <div class="col l12 m12 s12">
                  <a href="<?= base_url('Home/index');?>" class="btn waves-effect waves-light" style="font-size:12px;text-transform:capitalize;font-weight:700;width:100%;background:#d9d5c5;color:black;box-shadow:none;">Continue Shopping</a>
                </div>
              <?php endif;?>
              </div>
              <!-- button section end -->
          </div>
        </div>
        <!-- card section end -->
      </div>
    </div>
    <!-- cart section end -->
    <!-- footer section start -->
    <?php $this->load->view('Home/footer.php');?>
    <!-- footer section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php include('js.php');?>
    <!-- include js file include end -->
  </body>
</html>
