<?php $this->load->helper('product'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title><?= (count($category_details))?$category_details[0]->category_name:'Category Not Found' ;?> - Online Shop</title>
    <!-- css file include start -->
    <?php include('css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <!-- custom css file -->
    <style>
    .btn-flat:hover{
      background:black;;
      color: white;
    }
    #category_filter{
      padding-top: 10px;
      padding-bottom: 10px;
    }
    #category_filter li a{
      color:gray;
      font-size: 14px;
    }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- top & menu bar File start -->
    <?php include('top_menu_bar.php');?>
    <!-- top & menu bar File end-->
    <!-- card section start -->
      <div class="card" style="margin-top:10px;">
        <div class="card-content" style="padding:10px;border-bottom:1px solid silver;">
          <h5 style="margin-top:5px;font-weight:500;"><?= (count($category_details))?$category_details[0]->category_name . " Products" :'Category Not Found';?><span class="right">
          <button type="button" name="button" style="background:black;color:white;font-weight:500;text-transform:capitalize;width:102%;margin-right:10px;" class="btn btn-flat waves-effect waves-light dropdown-trigger"  data-target="category_filter">
          <span class="fa fa-filter"></span> Product Filter</button> </span> </h5>
          <!-- category filter dropdown start -->
          <?php
          $cat_id = (count($category_details)) ?$category_details[0]->id:"0";
          ?>
          <ul class="dropdown-content" id="category_filter">
            <li> <a href="<?=base_url('Home/Product_Filter/'.$cat_id.'/default')?>" class="waves-effect"> <span class="fa fa-cubes"></span> Default Product</a> </li>
            <li> <a href="<?=base_url('Home/Product_Filter/'.$cat_id.'/best_match')?>" class="waves-effect"> <span class="fa fa-cube"></span> Best Match</a> </li>
            <li> <a href="<?=base_url('Home/Product_Filter/'.$cat_id.'/lowest_price')?>" class="waves-effect"> <span class="fa fa-cube"></span> Lowest Price</a> </li>
            <li> <a href="<?=base_url('Home/Product_Filter/'.$cat_id.'/highest_price')?>" class="waves-effect"> <span class="fa fa-cube"></span> Highest Price</a> </li>
          </ul>
          <!-- category filter dropdown end-->
        </div>
        <div class="card-content" style="padding:0px;margin-top:10px;">
          <div class="row" style="margin-bottom:0px;">
            <?php if(count($products)):?>
              <?php foreach($products as $pro):?>
            <div class="col l2 m3 s6">
              <!-- card section start -->
              <a href="<?= base_url('Home/Product_Details/').$pro->id;?>" target="_blank">
                <div class="card waves-effect">
                  <div class="card-image">
                    <img src="<?= base_url().'uploads/product_image/'.$pro->image;?>" alt="" style="height:200px;width:100%;padding:5px">
                  </div>
                  <div class="card-content" style="border-Bottom:1px solid silver;padding:10px;">
                    <h6 style="font-size:15px;font-weight:500;margin-top:3px;color:black;" title="<?=$pro->product_title;?>"><?=word_limiter($pro->product_title,3);?></h6>
                    <h6 style="font-size:14px;color:gray;margin-top:5px;"><?php $category_data = get_category_product($pro->category_id);
                      echo (count($category_data))?$category_data[0]->category_name:"Category Not Found";?></h6>
                    <h5 style="font-size:20px;color:green;font-weight:500;margin-top:0px;margin-bottom:3px;"> <span class="fa fa-rupee-sign"></span>&nbsp;<?= $pro->price;?></h5>
                  </div>
                  <div class="card-content" style="padding:2px;">
                    <center>
                      <a  href="#!" data-position="top" data-tooltip="Add To Cart" onclick="add_to_cart('<?= $pro->id;?>')"class="btn btn-flat btn-floating waves-effect tooltipped"> <span class="fa fa-shopping-cart"></span> </a>
                      <a href="#!"  data-position="top" data-tooltip="View Prdouct"  onclick="product_detail_modal('<?= $pro->id;?>')" class="btn btn-flat btn-floating waves-effect tooltipped"> <span class="fa fa-eye"></span> </a>
                    </center>
                  </div>
                </div>
              </a>
              <!-- card section end -->
            </div>
          <?php endforeach;
            else:?>
              <h6 colspan="5" style="color:gray;font-weight:500;text-align:center;">Product Not Found.</h6>
          <?php endif;?>
          </div>
        </div>
      </div>
    <!-- card section end  -->
    <!-- footer section start -->
    <?php $this->load->view('Home/footer.php');?>
    <!-- footer section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php $this->load->view('Home/js.php');?>
    <?php include('js.php');?>
    <!-- include js file include end -->
  </body>
</html>
