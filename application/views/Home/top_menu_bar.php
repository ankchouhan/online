
  <!-- topbar section start -->
  <div id="topbar">
    <h6 style="font-size:14px;margin-top:5px;padding-left:15px;font-weight:500;"> <span class="fa fa-mobile-alt"></span>&nbsp;+91-8225994767&nbsp;&nbsp;<span class="fa fa-envelope-open-text">
    </span>&nbsp;info@Onlineshop.com <span class="right"><a style="color:black;padding-right:15px;" class="dropdown-trigger" data-target="my_account_dropdown"><span class="far fa-user"></span>&nbsp;My Account</a></span></h6>
    <!-- my account dropdown swction start -->
    <ul class="dropdown-content" id="my_account_dropdown">
      <?php if($this->session->userdata('email')=="" && $this->session->userdata('password')==""):?>
      <li><a href="<?= base_url('Home/User_Signup');?>" class="waves-effect"><span class="fa fa-user-plus"></span>&nbsp;Register</a> </li>
      <li><a href="<?= base_url('Home/User_Signin');?>" class="waves-effect"><span class="fa fa-sign-in-alt"></span>&nbsp;LogIn</a> </li>
    <?php else:?>
      <li><a href="<?= base_url('Home/Dashboard');?>" class="waves-effect"><span class="fa fa-home"></span>&nbsp;Dashboard</a> </li>
      <li><a href="<?= base_url('Home/carts');?>" class="waves-effect"><span class="fa fa-shopping-cart"></span>&nbsp;My Cart</a> </li>
      <li><a href="<?= base_url('Home/My_Order');?>" class="waves-effect"><span class="fa fa-truck"></span>&nbsp;My Order's</a> </li>
      <li><a href="<?= base_url('Home/User_Logout');?>" class="waves-effect"><span class="fa fa-sign-out-alt"></span>&nbsp;Logout</a> </li>
    <?php endif;?>
    </ul>
    <!-- my account dropdown swction end -->
  </div>
  <!-- topbar section end -->
  <!-- search bar section start -->
  <div class="bar">
  <div id="search_bar" class="search_bar">
    <div class="row" style="margin-bottom:0px;">
      <div class="col l3 m3 s12">
          <h6 style="margin-top:25px;"><a href="<?=base_url('Home/index');?>" class="brand-logo" id="logo"><span class="fab fa-shopware"></span>nline Shop</a> </h6>
      </div>
      <div class="col l6 m6 s12">
        <!-- search product form section start -->
        <ul id="search_form" class="search_form">
          <li>
            <input type="text" name="search_box" value="" id="search_box" placeholder="Search Your product">
            <!-- product list section start -->
            <ul id="show_product_list">

            </ul>
            <!-- product list section end -->
          </li>
          <li>
            <button type="submit" name="button" class="btn waves-effect waves-light" style="box-shadow:none;background:black;
            text-transform:capitalize;height:37px;font-weight:500;">Search Now</button>
          </li>
        </ul>
        <!-- search product form section end -->
      </div>
      <div class="col l3 m3 s12">
          <h6 style="color:black;font-size:16px;text-align:center;font-weight:500;margin-top:20px;margin-right:15px;line-height:20px;">
          <a style="color:black;"href="<?= base_url('Home/Carts');?>"> <span class="fa fa-shopping-cart"></span>&nbsp;Shopping Cart<br><span id="total_product">0</span>&nbsp;Items -
          <span class="fa fa-rupee-sign"></span> <span id="total_amount"></span> </a></h6>
      </div>
    </div>
  </div>
  <!-- search bar section end -->
  <!-- menu bar section start -->
  <nav>
    <div class="nav-wrapper">
      <!-- left side menu section start -->
        <ul class="left">
          <li><a href="<?= base_url('Home/index');?>">Home</a></li>
          <li><a href="#">Company Profile</a></li>
          <li><a href="<?= base_url('Home/Shipping_policy');?>">Our Policies</a></li>
          <li><a href="#">Offers</a></li>
        </ul>
      <!-- left side menu section end -->
    </div>
  </nav>
</div>
<!-- menu bar section end -->
<?php if($msg = $this->session->userdata('success')):?>
<!-- message section start -->
  <div class="card">
    <div class="card-content" style="padding:8px;padding-left:15px;">
      <h6 style="font-size:14px;font-weight:500;color:green;margin-top:5px;"><span class="fas fa-check-double"></span>&nbsp;<?= $msg;?></h6>
    </div>
  </div>
<!-- message section start -->
<?php endif;?>
<?php if($msg = $this->session->userdata('error')):?>
<!-- message section start -->
  <div class="card">
    <div class="card-content" style="padding:8px;padding-left:15px;">
      <h6 style="font-size:14px;font-weight:500;margin-top:5px;color:red;"><span class="fas fa-exclamation-triangle"></span>&nbsp;<?= $msg;?></h6>
    </div>
  </div>
<!-- message section start -->
<?php endif;?>
