<?php $this->load->helper('product'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Home - Online Shop</title>
    <!-- css file include start -->
    <?php include('css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <style media="screen">
      .btn-flat:hover{
        background:black;
        color:white;
      }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- top & menu bar File start -->
    <?php include('top_menu_bar.php');?>
    <!-- top & menu bar File end-->
    <!-- image slider section start -->
      <div class="carousel carousel-slider center">
        <div class="carousel-item red white-text" href="#one!">
          <img src="<?=base_url('assets/image/banner2.jpg');?>" alt="" class="responsive-img" style="max-height:400px;max-width:100%;">
        </div>
        <div class="carousel-item red white-text" href="#one!">
          <img src="<?=base_url('assets/image/banner3.png');?>" alt="" class="responsive-img" style="max-height:400px;max-width:100%;">
        </div>
      </div>
    <!-- image slider section end -->
    <!-- category section start -->
    <div class="row"style="margin-top:5px;">
      <?php if(count($categories)):?>
        <?php foreach($categories as $cat):?>
        <div class="col l3 m6 s12">
          <div class="card hoverable waves-effect" style="height:120px;width:100%;">
            <div class="card-content" style="padding:5px;margin-left:10px;">
              <div class="row" style="margin-bottom:0px;">
                <div class="col l7 m7 s7">
                  <h6 style="font-size:15px;font-weight:500;margin-top:3px;"><?= $cat->category_name;?></h6>
                  <?php $prodct_count = get_category_product($cat->id);?>
                  <h6 style="font-size:14px;color:gray;margin-top:5px;"><?= count($prodct_count);?>&nbsp;Products</h6>
                  <a href="<?= base_url('Home/Product_Categories/'.$cat->id);?>" class="btn waves-effect waves-light" style="background:black;box-shadow:none;margin-top:15px;"> View More</a>
                </div>
                <div class="col l5 m5 s5">
                  <img src="<?= base_url().'uploads/category_image/'.$cat->cat_image;?>" class="responsive-img" style="margin-top:5px;width:100%;height:100px;">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endforeach;
        else:?>
        <h6 colspan="5" style="color:gray;text-align:center;font-weight:bold;"> Category Not Found</h6>
    <?php endif;?>
    </div>
    <!-- category section start -->
    <?php if(count($categories)):?>
      <?php foreach($categories as $cat):?>
    <!-- category product list section start -->
      <div class="row">
          <?php $prodct_count = get_category_product($cat->id);?>
        <h5 style="font-weight:500;padding-left:10px;font-size:22px;"><?php
          if(count($prodct_count) == "0"){

            }
          else{
            echo $cat->category_name;
            echo "<span class='right'>";
            echo '<a type="button" href='.base_url("Home/Product_Categories/".$cat->id).' name="button"class="btn waves-effect waves-light" style="background:black;text-transform:capitalize;margin-right:12px;">View More</a> </span></h5>';
          };?>
          <?php
          $products = get_category_product($cat->id);
          if(count($products)):
            foreach($products as $pro):
          ?>
          <div class="col l2 m3 s6">
            <a href="<?= base_url('Home/Product_Details/'.$pro->id);?>" target="_blank">
            <div class="card waves-effect"  >
              <div class="card-image">
                <img src="<?= base_url().'uploads/product_image/'.$pro->image;?>" alt="" style="width:100%;height:200px;padding:5px;">
              </div>
              <div class="card-content" style="border-Bottom:1px solid silver;padding:10px;">
                <h6 style="font-size:15px;font-weight:500;margin-top:3px;color:black;" title="<?= $pro->product_title;?>"><?= word_limiter($pro->product_title,3);?></h6>
                <h6 style="font-size:14px;color:gray;margin-top:5px;"><?= $pro->category_name;?></h6>
                <h5 style="font-size:20px;color:green;font-weight:500;margin-top:0px;margin-bottom:3px;"> <span class="fa fa-rupee-sign"></span>&nbsp;<?= number_format($pro->price);?></h5>
              </div>
              <div class="card-content" style="padding:2px;">
                <center>
                  <a  href="#!" data-position="top" data-tooltip="Add To Cart" onclick="add_to_cart('<?= $pro->id;?>')" class="btn btn-flat btn-floating waves-effect tooltipped"><span class="fa fa-shopping-cart"></span> </a>
                  <a href="#!"  data-position="top" data-tooltip="View Product" onclick="product_detail_modal('<?= $pro->id;?>')" class="btn btn-flat btn-floating waves-effect tooltipped"> <span class="fa fa-eye"></span> </a>
                </center>
              </div>
            </div>
          </a>
        </div>
        <?php endforeach;
          else:?>
          <!-- <h6 colspan="5" style="margin-left: 5px;font-size: 14px;color:gray;font-weight:500;">Product Not Found</h6> -->
        <?php endif;?>
      </div>
    <!-- category product list section start -->
    <?php endforeach;
      else:?>
      <h6 colspan="5" style="text-align:center;color:gray;font-weight:500;">Category Not Found</h6>
    <?php endif;?>
    <!-- footer section start -->
    <?php $this->load->view('Home/footer.php');?>
    <!-- footer section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php include('js.php');?>
    <!-- include js file include end -->
    <script type="text/javascript">
    $(document).ready(function(){
      // serach sticky query
      var stickyNavTop = $('.bar').offset().top;
      var stickyNav = function(){
      var scrollTop = $(window).scrollTop();
        if (scrollTop > stickyNavTop) {
            $('.bar').addClass('sticky');
        }else {
            $('.bar').removeClass('sticky');
        }
      };
      stickyNav();
      $(window).scroll(function() {
        stickyNav();
      });
    });
    </script>
  </body>
</html>
