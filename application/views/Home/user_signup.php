<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>User SignUp - Online Shop</title>
    <!-- css file include start -->
    <?php include('css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <!-- custom css file -->
    <style>
      #input_box{
        border:1px solid silver;
        box-shadow: none;
        box-sizing: border-box;
        padding-left: 10px;
        padding-right: 10px;
        height:40px;
        border-radius: 2px;
      }
      #address{
        border:1px solid silver;
        padding:10px;
        outline: none;
        height:90px;
        resize: none;
      }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- top & menu bar File start -->
    <?php include('top_menu_bar.php');?>
    <!-- top & menu bar File end-->
    <!-- user sign up form section start -->
    <div class="row" style="margin-top:10px;margin-bottom:0px;">
      <div class="col l4 m4 s12"></div>
      <div class="col l4 m4 s12">
        <!-- card section start -->
        <div class="card">
          <div class="card-content">
            <?= form_open('Home/User_Register/'.$page);?>
            <center>
              <h5 style="margin-top:5px;"> <span class="fa fa-users"></span> </h5>
              <h6>Create Account</h6>
            </center>
            <h6 style="font-size:14px;color:gray;font-weight:500;" >Full Name</h6>
            <input type="text" name="full_name" id="input_box" placeholder="Enter Your Full Name" required>
            <h6 style="font-size:14px;color:gray;font-weight:500;">Email</h6>
            <input type="text" name="email" class="email" id="input_box" placeholder="Enter Your Email" required>
            <h6 style="font-size:14px;color:gray;font-weight:500;">Mobile no.</h6>
            <input type="text" name="mobile_no" id="input_box" placeholder="Enter Your Mobile No." required>
            <h6 style="font-size:14px;color:gray;font-weight:500;">Password</h6>
            <span style="display:flex;"><input type="password" name="password" onkeyup="check_password()" class="password" id="input_box" placeholder="Enter Your Password" required><i style="margin-top:10px;padding-left:5px;cursor:pointer;"class="fas fa-eye-slash" id="eye"></i></span>
            <h6 style="font-size:14px;color:gray;font-weight:500;">Re-type Password</h6>
            <input type="password" name="retype_password" id="input_box"  onkeyup="check_password()" class="password" placeholder="Re-Type Your Password" required>
            <h6 style="font-size:14px;color:gray;font-weight:500;">Address</h6>
            <textarea name="address" id="address" placeholder="Enter Your Address"></textarea>
            <button type="submit" name="button" class="btn waves-effect waves-light" id="btn_register_now" style="background:black;width:100%;text-transform: capitalize;margin-top:10px;box-shadow:none;">Register Now</button>
            <h6 style="font-size:14px;color:gray;font-weight:500;text-align:center;">I Have Already Account</h6>
            <a href="<?= base_url('Home/User_SignIn');?>"class="btn waves-effect" style="background:#206623;text-transform: capitalize;width:100%;margin-top:10px;box-shadow:none;">Sign In</a>
            <?= form_close();?>
          </div>
        </div>
        <!-- card section end -->
      </div>
      <div class="col l4 m4 s12"></div>
    </div>
    <!-- user sign up form section end -->
    <!-- footer section start -->
    <?php $this->load->view('Home/footer.php');?>
    <!-- footer section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php include('js.php');?>
    <!-- include js file include end -->
    <!-- custom js file include -->
    <script type="text/javascript">


        // check password script start
        function check_password()
        {
          var password = $('input[name ="password"]');
          var re_password = $('input[name ="retype_password"]');
          if(password.val().length > 6)
          {
            if(password.val() == re_password.val() || re_password.val() == password.val())
            {
              $('#btn_register_now').prop('disabled',false);

            }else {
              $('#btn_register_now').prop('disabled',true);
            }
          }else {
            $('#btn_register_now').prop('disabled',true);
          }
        }

        // check password script end

    </script>
  </body>
</html>
