<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>User SignIn- Online Shop</title>
    <!-- css file include start -->
    <?php include('css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <!-- custom css file -->
    <style>
      #input_box{
        border:1px solid silver;
        box-shadow: none;
        box-sizing: border-box;
        padding-left: 10px;
        padding-right: 10px;
        height:40px;
        border-radius: 2px;
      }
      #address{
        border:1px solid silver;
        padding:10px;
        outline: none;
        height:90px;
        resize: none;
      }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- top & menu bar File start -->
    <?php include('top_menu_bar.php');?>
    <!-- top & menu bar File end-->
    <!-- user signup form start -->
    <div class="row" style="margin-top:10px;margin-bottom:0px;">
      <div class="col l4 m4 s12"></div>
      <div class="col l4 m4 s12">
        <!-- card section start -->
        <div class="card">
          <div class="card-content">
            <?= form_open('Home/User_Login/'.$page);?>
            <center>
              <h5 style="margin-top:5px;"> <span class="fa fa-users"></span> </h5>
              <h6>SignIn Account</h6>
            </center>
            <h6 style="font-size:14px;color:gray;font-weight:500;">Username / Email</h6>
            <input type="text" name="email" class="email" id="input_box" placeholder="Enter Your Username / Email" autocomplete="off">
            <h6 style="font-size:14px;color:gray;font-weight:500;">Password</h6>
            <span style="display:flex;"><input type="password" name="password" class="password"id="input_box" placeholder="Enter Your Password"><i style="margin-top:10px;padding-left:5px;cursor:pointer;"class="fas fa-eye-slash" id="eye"></i></span>
            <button type="submit" name="button" class="btn waves-effect waves-light" style="background:#206623;width:100%;text-transform: capitalize;margin-top:10px;box-shadow:none;">Sign In</button>
            <h6 style="font-size:14px;color:gray;font-weight:500;text-align:center;">I don't have an Account</h6>
            <a href="<?= base_url('Home/User_Signup');?>" class="btn waves-effect" style="background:black;text-transform: capitalize;width:100%;margin-top:10px;box-shadow:none;">Create An Account</a><br><br>
            <center><a href="<?= base_url('Home/Forget_password');?>">Forget Password <span class="fa fa-question-circle"></span></a></center>
            <?= form_close();?>
          </div>
        </div>
        <!-- card section end -->
      </div>
      <div class="col l4 m4 s12"></div>
    </div>
    <!-- user signup form end -->
    <!-- footer section start -->
    <?php $this->load->view('Home/footer.php');?>
    <!-- footer section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php include('js.php');?>
    <!-- include js file include end -->
  </body>
</html>
