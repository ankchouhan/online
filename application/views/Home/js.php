
    <!-- custom css file -->
    <style media="screen">
      #product_detail_modal
      {
        width:80%;
        overflow-y: hidden;
      }
      .spinner-wrapper {
        position:fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 999999;
        backdrop-filter:blur(8px);
      }
      .spinner{
        position: absolute;
        top: 42%;
        left: 42%;
      }
    </style>
    <!-- product details modal section start -->
      <div class="modal" id="product_detail_modal" >

        </div>
    <!-- product details modal section end -->
    <!-- preloader section start -->
      <div id="preloader" style="display:none;">
        <div class="spinner-wrapper">
          <div class="spinner">
            <center>
            <img src="<?= base_url('assets/image/731.gif')?>" alt="">
            <h6 id="text"></h6>
          </center>
          </div>

        </div>
      </div>
    <!-- preloader section end -->
    <!-- include jquery js file  -->
    <script src="<?= base_url('assets/jquery/jquery.js');?>"></script>
    <!-- include materialize js file -->
    <script src="<?= base_url('assets/materialize/js/materialize.js');?>"></script>
    <!-- cancas js file include -->
    <script src="<?= base_url('assets/materialize/js/canvas.js');?>"></script>
    <!-- custom js file inclue -->
    <script type="text/javascript">
      $(document).ready(function(){

        function ValidateEmail(email){
          var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
          }
          $('.email').keyup(function(){
          if (!ValidateEmail($('.email').val())){
            $('#btn_register_now').prop('disabled',true);
          }
          else {
          $('#btn_register_now').prop('disabled',false);
          }
        });

        calculate_cart_product();
        // tooltipped script
		 $('.tooltipped').tooltip();
		 // tooltipped script
        // modal section
        $('.modal').modal({
          dismissible:false,
        });
        // $('#product_detail_modal').modal('open');

        // collapsible trigger
        $('.collapsible').collapsible();

        // sidenav Trigger
        $('.sidenav').sidenav({
        });
        // dropdown trigger
        $('.dropdown-trigger').dropdown({
          coverTrigger:false,
          hover:true,
        });

        // image slider
        $('.carousel.carousel-slider').carousel({
          fullWidth: true,
          indicators: true
        });

        // // serach sticky query
        // var stickyNavTop = $('.search_bar').offset().top;
        // var stickyNav = function(){
        // var scrollTop = $(window).scrollTop();
        // if (scrollTop > stickyNavTop) {
        //     $('.search_bar').addClass('sticky');
        // } else {
        //     $('.search_bar').removeClass('sticky');
        //
        // }
        // };
        // stickyNav();
        // $(window).scroll(function() {
        //   stickyNav();
        // });
      });
    </script>

    <!-- custom ajax script  for product modal-->
    <script type="text/javascript">
        function product_detail_modal($product_id)
        {
          $.ajax({
            type:'ajax',
            method:'get',
            url:"<?= base_url('Home/product_detail_modal/');?>"+$product_id,
            beforeSend:function(){
              $('#preloader').show();

            },
            success:function(data){
              $('#preloader').hide();
              $('#product_detail_modal').modal('open')
              $('#product_detail_modal').html(data);
            },
            error:function(){
              alert('Error ! Fetch Product Details');
            },

          });
        }

        // add to cart with ajax
        function add_to_cart($product_id)
        {
          $.ajax({
            type:'ajax',
            method:'get',
            url:"<?= base_url('Home/add_to_cart/');?>"+$product_id,
            beforeSend:function(data){
              $('#preloader').show();
              $('#text').text('Product Add in Your Cart....');
            },
            success:function(data){
              $('#preloader').hide();
              if(data == '1')
              {
                M.toast({html:'Product Successfully Add to Cart'});
                calculate_cart_product();
              }else {
                M.toast({html:'Product Not Add in Cart'});
              }
            },
            error:function(){
              alert('Error ! Add To Cart Product');
            },
          });
        }
        // update quantity script Start
        function update_quantity(type,id)
        {
           var quantity = $('#qty_'+id).val();
           $.ajax({
             type:'ajax',
             method:'get',
             url:"<?= base_url('Home/Update_Quantity/');?>"+ quantity +'/'+type+'/'+id,
             beforeSend:function(data){
               $('#preloader').show();
               $('#text').text('Update Product Quantity....');
             },
             success:function(data){
               $('#preloader').hide();
               if(data == '1')
               {
                 M.toast({html:'Update Product Qunatity Successfully..'});
                 location.reload();
               }else {
                 M.toast({html:'Update Product Quantity Fail..'});
               }
             },
             error:function(){
               alert('Error ! Update Quantity..');
             },
           });
        }

        // calculate cart product script section start on desktop
        function calculate_cart_product()
        {
           $.ajax({
            type:'ajax',
            method:'get',
            url:"<?= base_url('Home/calculate_cart_products/');?>",
            success:function(data){
              var json_data = JSON.parse(data);
              $('#total_product').html(json_data.total_product);
              $('#total_amount').html(json_data.total_amount);
            },
            error:function(){
              alert('Error ! Update Quantity..');
            },
          });
        }
        // calculate cart product script section start on desktop

        // search Product start
        $('body').click(function(){
          $('#show_product_list').hide();
        });
        $('#search_box').keyup(function(){
          var search_box = $('#search_box');
          if(search_box.val().length >0){
            $.ajax({
             type:'ajax',
             method:'post',
             url:"<?= base_url('Home/Search_Products/');?>",
             data:{search_box:search_box.val()},
             success:function(data){
               $('#show_product_list').show();
               $('#show_product_list').html(data);
             },
             error:function(){
               alert('Error ! Product Search');
             }
           });
         }else{
           $('#show_product_list').hide();
         }
       });
        // search Product end

        // show password start
        $('#eye').click(function(){
        if($(this).hasClass('fa-eye-slash')){
          $(this).removeClass('fa-eye-slash');
          $(this).addClass('fa-eye');
          $('.password').attr('type','text');
        }else{
          $(this).removeClass('fa-eye');
          $(this).addClass('fa-eye-slash');
          $('.password').attr('type','password');
        }
      });
        // show password end
    </script>
