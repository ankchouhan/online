
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Place Order - Online Shop</title>
    <!-- css file include start -->
    <?php include('css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <!-- custom css file -->
    <style>
      #input_box{
        border:1px solid silver;
        box-shadow: none;
        box-sizing: border-box;
        padding-left: 10px;
        padding-right: 10px;
        height:40px;
        border-radius: 2px;
      }
      #shipping_address{
        border:1px solid silver;
        padding:10px;
        outline: none;
        height:90px;
        resize: none;
      }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- top & menu bar File start -->
    <?php include('top_menu_bar.php');?>
    <!-- top & menu bar File end-->
      <div class="container">
        <!-- card section start -->
        <div class="card">
          <div class="card-content" style="border-bottom:1px solid silver;padding:10px;">
            <h6 style="font-weight:500;margin-top:0px;">User Login & Register</h6>
          </div>
          <?php if($this->session->userdata('email')== "" && $this->session->userdata('password') == "" ):?>
          <div class="card-content" style="padding-top:0px;">
            <!-- button section start -->
            <div class="row" style="margin-top:18px;margin-bottom:0px;">
              <div class="col l6 m6 s12">
                <a href="<?= base_url('Home/User_Signup/cart');?>" class="btn waves-effect waves-light" style="font-size:12px;text-transform:capitalize;font-weight:500;width:100%;background:#d9d5c5;color:black;box-shadow:none;">Register Account</a>
              </div>
              <div class="col l6 m6 s12">
                <a href="<?= base_url('Home/User_Signin/cart');?>" class="btn waves-effect waves-light" style="font-size:12px;text-transform:capitalize;font-weight:500;width:100%;background:black;">Login Account</a>
              </div>
            </div>
            <!-- button section end -->
          </div>
        <?php endif;?>
        <div class="card-content" style="border-bottom:1px solid silver;padding:10px;">
          <h6 style="font-weight:500;margin-top:0px;">Shipping Address</h6>
        </div>
        <?php if($this->session->userdata('email')!= ""):?>
        <?php
        $this->load->helper('users');
        $user = get_user_details($this->session->userdata('email'),$this->session->userdata('password'));
        $check_address = get_temp_address($user[0]->id);
        if(count($check_address)):
          $address = $check_address[0]->address;
        else:
          $address = $user[0]->address ;?>
        <?php endif;?>
        <div class="card-content" style="padding-top:0px;">
          <?= form_open('Home/Save_Temp_Address/'.$user[0]->id);?>
          <h6 style="font-size:14px;color:gray;font-weight:bold;">Shipping Address</h6>
          <textarea name="shipping_address" id="shipping_address" placeholder="Enter Your Address"><?= $address ;?></textarea>
          <button type="submit" class="btn waves-effect waves-light" style="background:black;text-transform:capitalize;margin-top:10px; " name="button">Save Address</button>
          <?= form_close();?>
        </div>
      <?php endif;?>
      <div class="card-content" style="border-bottom:1px solid silver;padding:10px;">
        <h6 style="font-weight:500;margin-top:0px;">Complete Purchase</h6>
      </div>
      <?php if(count($products)>0):?>
      <div class="card-content" style="padding-top:0px;">
        <?php
        $t_amount = "0";
        if((count($products))):?>
          <?php foreach($products as $pro):
            $t_amount += ($pro->quantity * $pro->rate);
            ?>
        <h6 style="font-size:14px;color:gray;font-weight:500;" title="<?= $pro->product_name;?>"><?= word_limiter($pro->product_name,10);?>&nbsp;&nbsp;<b>X</b>&nbsp;<?= $pro->quantity ;?>&nbsp;Items<span class="right" style="color:black;">
            <span class="fa fa-rupee-sign"></span>&nbsp;<?php $sum_amount = $pro->rate * $pro->quantity; echo number_format($sum_amount);?></span> </h6>
      <?php endforeach;
          else:
            $t_amount = "0";?>
          <h6 style="text-align:center;color:gray;font-weight:500;">Product Not Found</h6>
      <?php endif;?>
      <h6 style="border-top:2px dashed silver;padding:5px;font-weight:500;">Grand total:<span class="right"> <span class="fa fa-rupee-sign"></span>&nbsp;<?= number_format($t_amount);?></span></h6>
      <a href="<?= base_url('Home/Complete_Purchased');?>" class="btn waves-effect waves-light" style="background:black;text-transform:capitalize;margin-top:10px;">Complete Purchase</a>
      </div>
    <?php endif;?>
      </div>
        <!-- card section end -->
      </div>
    <!-- footer section start -->
    <?php $this->load->view('Home/footer.php');?>
    <!-- footer section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php include('js.php');?>
    <!-- include js file include end -->
  </body>
</html>
