<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Forget Password- Online Shop</title>
    <!-- css file include start -->
    <?php include('css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <!-- custom css file -->
    <style>
      #input_box{
        border:1px solid silver;
        box-shadow: none;
        box-sizing: border-box;
        padding-left: 10px;
        padding-right: 10px;
        height:40px;
        border-radius: 2px;
      }
      #address{
        border:1px solid silver;
        padding:10px;
        outline: none;
        height:90px;
        resize: none;
      }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- top & menu bar File start -->
    <?php include('top_menu_bar.php');?>
    <!-- top & menu bar File end-->
    <!-- user signup form start -->
    <div class="row" style="margin-top:10px;margin-bottom:0px;">
      <div class="col l4 m4 s12"></div>
      <div class="col l4 m4 s12">
        <!-- card section start -->
        <div class="card" id="enter_email">
          <div class="card-content">
            <center>
              <h5 style="margin-top:5px;"> <span class="fa fa-user-lock"></span> </h5>
              <h6>Reset Password</h6>
            </center>
            <h6 style="font-size:14px;color:gray;font-weight:500;">Enter Your Email:</h6>
            <input type="text" name="email" id="input_box" class="email" placeholder="Enter Your Username / Email"  autocomplete="off">
            <button type="submit" name="button" class="btn waves-effect waves-light"  id="btn_email" style="background:black;width:100%;text-transform: capitalize;margin-top:10px;box-shadow:none;">Confirm Email</button>
            <a href="<?= base_url('Home/User_Signin');?>" class="btn waves-effect" style="background:red;text-transform: capitalize;width:100%;margin-top:10px;box-shadow:none;">Cancel</a>
          </div>
        </div>
        <!-- card section end -->
        <!-- card section start -->
        <div class="card" id="password">
          <div class="card-content">
            <?= form_open('Home/change_password');?>
            <center>
              <h5 style="margin-top:5px;"> <span class="fa fa-unlock-alt"></span> </h5>
              <h6>Reset Password</h6>
            </center>
            <h6 style="font-size:14px;color:gray;font-weight:500;">Your Password:</h6>
            <input type="text" name="password" id="input_box" class="password" placeholder="Enter Your Password">
            <button type="submit" name="button" class="btn waves-effect waves-light" style="background:black;;width:100%;text-transform: capitalize;margin-top:10px;box-shadow:none;">Change Password</button>
            <a href="<?= base_url('Home/Forget_password');?>" class="btn waves-effect" style="background:red;text-transform: capitalize;width:100%;margin-top:10px;box-shadow:none;">Cancel</a>
            <?= form_close();?>
          </div>
        </div>
        <!-- card section end -->
      </div>
      <div class="col l4 m4 s12"></div>
      <!-- confirm modal -->
      <div class="modal" id="confirm_msg" style="width:40%;background:white;">
        <div class="modal-content" style="padding:10px;">
          <h5 style="font-weight:500;text-align:center;font-size:19px;"><span class="fa fa-check-double" ></span>&nbsp;Your password Has to be Send On your Email ID<span class="right modal-close" style="background:red;color:white;padding:7px;margin-top:0px;"><b>X</b></span></h5>
          <center>
            <img src="<?= base_url('assets/image/gmail.gif');?>" style="width:200px;height:200px;"alt="">
          </center>
        </div>
      </div>
    </div>
    <!-- user signup form end -->
    <!-- footer section start -->
    <?php $this->load->view('Home/footer.php');?>
    <!-- footer section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php include('js.php');?>
    <!-- include js file include end -->
    <script type="text/javascript">
        $(document).ready(function(){

          $('#password').hide();
          // check email start
          $('#btn_email').click(function(){
          var email = $('.email').val();
           if(email == "")
           {
             M.toast({html:'Please Enter Your Email.'});
           }else {
             $.ajax({
               type:'ajax',
               method:'post',
               url:"<?= base_url('Home/Confirm_Email')?>",
               data:{email:email},
               success:function(data){
                   var json_data = JSON.parse(data);
                   if(json_data.password == "0")
                   {
                     $('#enter_email').show();
                     $('#password').hide();
                     M.toast({html:'Your Email is Incorrect'});
                   }else{
                     $('#enter_email').hide();
                     $('#password').show();
                    $('.password').val(json_data.password);
                    }
                  },
               error:function(){
                 alert('Error ! Password Change Fail.');
               }
             });
           }
          // check email end
        });
      });
    </script>
  </body>
</html>
