<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>User SignUp - Online Shop</title>
    <!-- css file include start -->
    <?php include('css.php');?>
    <!-- css file include end -->
    <!-- custom css file include -->
    <!-- custom css file -->
    <style>
    .btn-flat:hover{
      background:#7a745c;
      color: white;
    }
    #category_filter{
      padding-top: 10px;
      padding-bottom: 10px;
    }
    #category_filter li a{
      color:gray;
      font-size: 14px;
    }
    </style>
  </head>
  <body>
    <!-- body section start -->
    <!-- top & menu bar File start -->
    <?php include('top_menu_bar.php');?>
    <!-- top & menu bar File end-->
    <!-- dashboard section start -->
    <div class="container">
      <div class="card">
        <div class="card-content" style="padding:10px;border-bottom:1px solid silver;">
          <h5 style="font-weight:500;font-size:20px;">Shipping Policy</h5>
        </div>
        <div class="card-content">
          <h6 style="font-weight:500;">Policy Heading</h6>
          <p style="font-size:14px;line-height:20px;text-align:justify;">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
             sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
             Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
              Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
              Excepteur sint occaecat cupidatat non proident,
             sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <h6 style="font-weight:500;margin-top:10px;">Shipping methods and delivery times</h6>
          <p style="font-size:14px;line-height:20px;text-align:justify;">Whether you offer one or several shipping methods, list out the different
            methods and the expected delivery times for each.Many times this is shown using a simple table format with
            one column displaying the different methods and another one with expected time frames. </p>
          <h6 style="font-weight:500;margin-top:10px;">Missing or lost package</h6>
          <p style="font-size:14px;line-height:20px;text-align:justify;">There are several reasons why a package gets lost or becomes a delivery exception,
             and most of the times it is out of your control. But directing customers on what to do if a package goes missing
             creates peace of mind. You can do this by easily displaying contact information and directions on how to report missing or lost packages.  </p>
          <h6 style="font-weight:500;margin-top:10px;">Return and exchange information</h6>
          <p style="font-size:14px;line-height:20px;text-align:justify;">It is advised that you update your shipping policy every so often.
             As your business grows, your shipping policy will change. For instance,if you go from self-fulfilling orders to outsourcing fulfillment, your shipping timelines will shift.
              We recommend you update your shipping policy every six months, and each time you have made a change to your shipping method(s) — whether it’s a new 3PL or expanding into more fulfillment center locations. </p>
          <h6 style="font-weight:500;margin-top:10px;">Shipping costs</h6>
          <p style="font-size:14px;line-height:20px;text-align:justify;">Displaying shipping costs gives your customers a chance to review and
            calculate their total costs before they shop around. Showcasing shipping costs
             becomes even more important for international customers who may not qualify for
             certain offers such as free shipping. Many times, international shipping varies between retailers significantly due
              to increase in carrier charges as well, so you’ll want to be as transparent as possible.  </p>
        </div>
      </div>
    </div>
    <!-- dashboard section end -->
    <!-- footer section start -->
    <?php $this->load->view('Home/footer.php');?>
    <!-- footer section end -->
    <!-- body section end -->
    <!-- include js file include start -->
    <?php include('js.php');?>
    <!-- include js file include end -->
  </body>
</html>
