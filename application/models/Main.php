<?php
  /**
   *
   */
  class Main extends CI_Model
  {
    public function __construct()
    {
      parent ::__construct();
    }
    public function login_auth($table,$args)
    {
      $fetch_rec = $this->db->get_where($table,$args);
      if($fetch_rec->num_rows>0)
      {
        return $fetch_rec->result();
      }else {
        return $fetch_rec->result();
      }
    }
    public function insert_data($table,$data)
    {
      $insert_data = $this->db->insert($table,$data);
      if($this->db->affected_rows()>0)
      {
        return true;
      }else {
        return false;
      }
    }
    public function fetch_rec_by_args($table,$args)
    {
      $this->db->order_by('id','desc');
      $fetch_rec = $this->db->get_where($table,$args);
      if($fetch_rec->num_rows()>0)
      {
        return $fetch_rec->result();
      }else {
        return $fetch_rec->result();
      }
    }
    public function fetch_rec_by_args_rand($table,$args)
    {
      $this->db->order_by('id','RANDOM');
      $fetch_rec = $this->db->get_where($table,$args);
      if($fetch_rec->num_rows()>0)
      {
        return $fetch_rec->result();
      }else {
        return $fetch_rec->result();
      }
    }
    public function fetch_all_records($table,$order,$limit)
    {
      if($limit == "limit")
      {

      }else {
        $this->db->limit($limit);
      }
      $fetch_rec = $this->db->select()
                ->from($table)
                ->order_by('id',$order)
                ->get();
      if($fetch_rec->num_rows()>0)
      {
        return $fetch_rec->result();
      }else {
        return $fetch_rec->result();
      }
    }
    public function delete_rec_by_args($table,$args)
    {
      $delete_rec = $this->db->delete($table,$args);
      if($this->db->affected_rows()>0)
      {
        return true;
      }else {
        return false;
      }
    }
    public function update_rec_by_args($table,$data,$args)
    {
      $update_rec = $this->db->where($args)
                ->update($table,$data);
      if($this->db->affected_rows()>0)
      {
        return true;
      }else {
        return false;
      }
      echo $this->db->last_query();
    }
    public function fetch_rec_by_args_with_like($table,$args)
    {
      $fetch_rec = $this->db->select()
                ->from($table)
                ->like($args)
                ->get();
      if($fetch_rec->num_rows()>0)
      {
        return $fetch_rec->result();
      }else {
        return $fetch_rec->result();
      }
    }
    public function ajax_category_search($query,$filter)
    {
      $fetch_rec = $this->db->select()
                ->from('category');
      if($filter == "new_category")
      {
        $this->db->like('category_name',$query)
                  ->order_by('id','desc');
      }elseif ($filter == "old_category") {
        $this->db->like('category_name',$query)
                  ->order_by('id','asc');
      }elseif ($filter == "highest_product") {
        $this->db->like('category_name',$query)
                  ->order_by('products','desc');
      }elseif ($filter == "lowest_product") {
        $this->db->like('category_name',$query)
                  ->order_by('products','asc');
      }elseif ($filter == "") {
        $this->db->like('category_name',$query)
                  ->order_by('id','desc');
      }else{
        $this->db->order_by('id','desc');
      }
      return $this->db->get();
    }
    public function fetch_rec_by_order($table,$args)
    {
      extract($args);
      $fetch_rec = $this->db->select()
                ->from($table)
                ->order_by($args['column_name'],$args['order'])
                ->get();
      if($fetch_rec->num_rows()>0)
      {
        return $fetch_rec->result();
      }else {
        return $fetch_rec->result();
      }
    }

    public function fetch_rec_by_arg_with_order($table,$args,$order_format)
    {
      extract($order_format);
      $fetch_rec = $this->db->select()
                ->from($table)
                ->where($args)
                ->order_by($order_format['column_name'],$order_format['order'])
                ->get();
      if($fetch_rec->num_rows()>0)
      {
        return $fetch_rec->result();
      }else {
        return $fetch_rec->result();
      }
    }
    public function fetch_rec_by_args_with_limit($table,$args,$limit)
    {
      $this->db->limit($limit);
      $fetch_rec = $this->db->get_where($table,$args);
      if($fetch_rec->num_rows()>0)
      {
        return $fetch_rec->result();
      }else {
        return $fetch_rec->result();
      }
    }
    public function ajax_product_search($query,$filter,$offset,$limit)
    {
      $fetch_rec = $this->db->select()
                ->from('product');
      if($filter == "new_category")
      {
        $this->db->like('product_title',$query)
                  ->order_by('id','desc')
                  ->limit($limit,$offset);
      }elseif ($filter == "old_category") {
        $this->db->like('product_title',$query)
                  ->order_by('id','asc')
                  ->limit($limit,$offset);
      }elseif ($filter == "highest_price") {
        $this->db->like('product_title',$query)
                  ->order_by('price','desc')
                  ->limit($limit,$offset);
      }elseif ($filter == "lowest_price") {
        $this->db->like('product_title',$query)
                  ->order_by('price','asc')
                  ->limit($limit,$offset);
      }elseif ($filter == "") {
        $this->db->like('product_title',$query)
                  ->order_by('id','desc')
                  ->limit($limit,$offset);
      }else{
        $this->db->order_by('id','desc')
                  ->limit($limit,$offset);

      }
      return $this->db->get();
    }
    public function insert_data_with_last_id($table,$data)
    {
      $insert_rec = $this->db->insert($table,$data);
      if($this->db->affected_rows()>0)
      {
        return $this->db->insert_id();
      }else {
        return 0;
      }
    }
    public function count($table)
    {
      $count = $this->db->select('*')
               ->from($table)
               ->get();
      if($count->num_rows()>0)
      {
        return $count->result();
      }else {
        return $count->result();
      }
    }
    public function ajax_order_search($query)
    {
      $fetch_rec = $this->db->select()
                            ->from('orders')
                            ->like('id',$query)
                            ->or_like('user_name',$query)
                            ->order_by('id','desc')
                            ->get();
      return $fetch_rec;
    }
    public function fetch_all_records_with_order($table,$order_format,$limit)
    {
      if($limit == "limit")
      {

      }else {
        $this->db->limit($limit);
      }
      extract ($order_format);
      $fetch_rec = $this->db->select()
                ->from($table)
                ->order_by($order_format['column_name'],$order_format['order'])
                ->get();
      if($fetch_rec->num_rows()>0)
      {
        return $fetch_rec->result();
      }else {
        return $fetch_rec->result();
      }
    }
    public function ajax_delivered_order($query)
    {
                    $this->db->order_by('id','desc')
                             ->like('id',$query);
        $fetch_rec = $this->db->get_where('orders',['order_status'=>"Delivered"]);
        return $fetch_rec;
    }
    public function fetch_all_sales($args)
    {
      $fetch_rec = $this->db->select('order_date,COUNT(order_date),SUM(total_quantity),SUM(total_amount)')
                ->from('orders')
                ->where($args)
                ->group_by('order_date')
                ->get();
      if($fetch_rec->num_rows()>0)
      {
        return $fetch_rec->result_array();
      }else {
        return $fetch_rec->result_array();
      }
    }
    public function ajax_pending_order($query)
    {
                    $this->db->order_by('id','desc')
                              ->like('id',$query);
      $fetch_rec = $this->db->get_where('orders',['order_status!='=>"Delivered"]);
      return $fetch_rec;
    }
    public function view_all_customer()
    {
      $fetch_rec = $this->db->select("*")
                            ->from('users')
                            ->get();
      if($fetch_rec->num_rows()>0)
      {
        return $fetch_rec->result();
      }else {
        return $fetch_rec->result();
      }
    }
  }
?>
