  <?php
  /**
   *
   */
  class Admin extends CI_Controller
  {
    public function __construct()
    {
      parent :: __construct();
      // Main Modal Class
      $this->load->model('Main',"cm");
      $this->load->helper('product');
    }
    public function index()
    {
      if($this->session->userdata('admin_id')==""){
        $this->load->view('Admin/login');
      }else {
        return redirect('Admin/Dashboard');
      }
    }
    public function Login()
    {
      if($this->session->userdata('admin_id')==""){
        $args = [
          'username' => $this->input->post('username'),
          'password' =>$this->input->post('password'),
        ];
        $result = $this->cm->login_auth('admin',$args);
        if(count($result)){
          $admin_session = [
            'admin_id' => $result[0]->id,
            'admin_fullname' => $result[0]->fullname,
            'admin_username' => $result[0]->username
          ];
          $this->session->set_userdata($admin_session);
          echo "1";
        }else {
          echo "0";
        }
      }else {
        return redirect('Admin/Dashboard');
      }
    }
    public function Dashboard()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $order = [
          'column_name'      => 'count_sale',
          'order'             =>'desc',
        ];
          $data['customer']  =$this->cm->count('users');
          $data['top_sold']    = $this->cm->fetch_all_records_with_order('product',$order,'6');
          $data['products'] = $this->cm->count('product');
          $data['category'] = $this->cm->count('category');

          // chart data
          $args = [
            'order_date' => date('Y-m-d')
          ];
          $today_chart = $this->cm->fetch_rec_by_args('orders',$args);
          $args = [
            'order_date' => date('Y-m-d',strtotime('-1 day'))
          ];
          $yesterday_chart = $this->cm->fetch_rec_by_args('orders',$args);
          $args = [
            'order_date' => date('Y-m-d',strtotime('-2 day'))
          ];
          $third_chart = $this->cm->fetch_rec_by_args('orders',$args);
          $args = [
            'order_date' => date('Y-m-d',strtotime('-3 day'))
          ];
          $fourth_chart = $this->cm->fetch_rec_by_args('orders',$args);
          $args = [
            'order_date' => date('Y-m-d',strtotime('-4 day'))
          ];
          $fifth_chart = $this->cm->fetch_rec_by_args('orders',$args);
          $args = [
            'order_date' => date('Y-m-d',strtotime('-5 day'))
          ];
          $six_chart = $this->cm->fetch_rec_by_args('orders',$args);
          $args = [
            'order_date' => date('Y-m-d',strtotime('-6 day'))
          ];
          $seven_chart = $this->cm->fetch_rec_by_args('orders',$args);

          $data['chart'] = [
            'today_order'          =>count($today_chart),
            'yesterday_order'      =>count($yesterday_chart),
            'third_order'          =>count($third_chart),
            'fourth_order'         =>count($fourth_chart),
            'fifth_order'          =>count($fifth_chart),
            'six_order'            =>count($six_chart),
            'seven_order'          =>count($seven_chart),
          ];
        $this->load->view('Admin/dashboard',$data);
      }
    }
    public function Signout()
    {
      $this->session->unset_userdata('admin_id');
      $this->session->unset_userdata('admin_fullname');
      $this->session->unset_userdata('admin_username');
      return redirect('Admin/index');
    }
    public function Add_Category()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $args = [
          'date>=' => date('Y-m-d',strtotime("-7 Days"))
        ];
        $data['categories'] = $this->cm->fetch_rec_by_args('category',$args);
        $this->load->view('Admin/add_category',$data);
      }
    }
    public function Upload_Category()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $config = [
          'upload_path' => './uploads/category_image',
          'allowed_types' => 'jpg|jpeg|gif|png',
        ];
        $this->load->library('upload',$config);
        $this->upload->do_upload('category_image');
        $img = $this->upload->data('file_name');
        $data = [
          'category_name' =>$this->input->post('category_name'),
          'cat_image'     => $img,
          'status'        =>'0',
          'date'          =>date('Y-m-d')
        ];
        if ($data['category_name'] == "" && $data['cat_image'] == "") {
          $this->session->set_flashdata('error','Please Enter Required Information.');
        }else {
          $result = $this->cm->insert_data('category',$data);
          if($result == true){
            $this->session->set_flashdata('success','Congretulation ! Category Insert Successfully.');
          }else {
            $this->session->set_flashdata('fail','Fail ! Category Insert');
          }
        }
        return redirect('Admin/Add_Category');
      }
    }
    public function Manage_Category()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $data['categories'] = $this->cm->fetch_all_records('category','desc','limit');
        $this->load->view('Admin/manage_category',$data);
      }
    }
    public function Delete_Category($id)
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        if($id == ""){
          $this->session->set_flashdata('error',"Please pass Category ID.");
        }else{
            $args = [
              'id' =>$id
            ];
            $category =$this->cm->fetch_rec_by_args('category',$args);
            // Delete image From Folder
            unlink('uploads/category_image/'.$category[0]->cat_image);

            $result = $this->cm->delete_rec_by_args('category',$args);
            if($result == true ){
              $this->session->set_flashdata('success',"Congretulation ! Delete Category Successfully.");
            }else {
              $this->session->set_flashdata('error',"Fail ! Category Delete.");
            }
          }
          if($this->uri->segment('4') == "1"){
            return redirect('Admin/Add_Category');
          }else {
            return redirect('Admin/Manage_Category');
        }
      }
    }
    public function Delete_Product($id)
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        if($id == ""){
          $this->session->set_flashdata('error',"Please pass Category ID.");
        }else{
            $args = [
              'id' =>$id
            ];
            $product =$this->cm->fetch_rec_by_args('product',$args);
            // Delete image From Folder
            unlink('uploads/product_image/'.$product[0]->image);
            $result = $this->cm->delete_rec_by_args('Product',$args);
            if($result == true ){
              $this->session->set_flashdata('success',"Congretulation ! Delete Category Successfully.");
            }else {
              $this->session->set_flashdata('error',"Fail ! Category Delete.");
            }
          }
            return redirect('Admin/Manage_Product');
        }
    }
    public function Edit_Category($id)
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        if($id == ""){
          $this->session->set_flashdata('error',"Please pass Category ID.");
        }else{
            $args = [
              'id' => $id
            ];
            $data['category'] = $this->cm->fetch_rec_by_args('category',$args);
            $args = [
              'date>=' => date('Y-m-d',strtotime("-7 Days"))
            ];
            $data['categories'] = $this->cm->fetch_rec_by_args('category',$args);

            $this->load->view('Admin/edit_category',$data);
        }
      }
    }
    public function Update_Category($id){
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $config = [
          'upload_path' => './uploads/category_image',
          'allowed_types' => 'jpg|jpeg|gif|png',
        ];
        $this->load->library('upload',$config);
        // check image
        if ($_FILES['category_image']['name'] == "") {

        }else {
          $args = [
            'id' =>$id,
          ];
          $old_data = $this->cm->fetch_rec_by_args('category',$args);
          // unlink old image
          unlink('uploads/category_image/'.$old_data[0]->cat_image);
          // unlink old image
          $this->upload->do_upload('category_image');
          $img = $this->upload->data('file_name');
          $data['cat_image'] = $img;
        }
        $data['category_name'] = $this->input->post('category_name');
        if ($data['category_name'] == "") {
          $this->session->set_flashdata('error','Please Enter Category Name.');
        }else {
          $args = [
            'id' => $id,
          ];
          $result = $this->cm->update_rec_by_args('category',$data,$args);
          if($result == true){
            $this->session->set_flashdata('success','Congretulation ! Category Update Successfully.');
          }else {
            $this->session->set_flashdata('fail','Fail ! Category Insert');
          }
        }
        return redirect('Admin/Edit_Category/'.$id);
      }
    }
    public function Ajax_Category_Search()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
      $output = "";
      $query = "";
      $filter = $this->input->post('filter');
      if($this->input->post('query'))
      {
        $query = $this->input->post('query');
      }
      $data = $this->cm->ajax_category_search($query,$filter);
      $output ="
                  <table style='margin-top:0px;' class='table striped'>
                    <tr>
                      <th class='center'>Image</th>
                      <th>Name</th>
                      <th>Products</th>
                      <th>Status</th>
                      <th class='center'>Action</th>
                    </tr>";
      if($data->num_rows()>0)
      {
        foreach($data->result() as $row)
        {
          $data = get_all_product($row->id);
          $output .="
          <tr>
            <td>
              <center>
                <img src='".base_url()."uploads/category_image/".$row->cat_image."'class='responsive-img' id='category_image'>
              </center>
            </td>
            <td style='font-size:14px;color:gray;'>".$row->category_name." <br> <a href='".base_url('Home/index')."' style='font-size:13px;'>View On Home</a> </td>
            <td style='font-size:14px;color:gray;'><a style='color:gray;'href='#'>".count($data)."</a> </td>
            <td style='font-size:14px;color:gray;'>".($row->status == "0"? '<a href="'.base_url('Admin/Change_Status/').$row->id.'/1'.'" style="color:gray;" title="Change Status"><span class="fa fa-toggle-on" style="font-size:13px;color:green;"></span> Active</a>'
            :'<a href="'.base_url('Admin/Change_Status/').$row->id.'/0'.'"  title="Change Status" style="color:gray;"><span class="fas fa-toggle-off" style="font-size:13px;color:red;"></span> Inactive</a>')."</td>
            <td class='center'><a href='".base_url('Admin/Edit_Category/'.$row->id)."' style='color:gray;'title='Edit'><span class='fas fa-edit'></span></a> -
              <a href='".base_url('Admin/Delete_Category/'.$row->id)."'  style='color:gray;'title='Delete'> <span class='fa fa-trash-alt'></span> </a> </td>
          </tr>
          <ul class='dropdown-content action_dropdown' id='action_dropdown'>
          <li>ankit</li>
          </ul>";
        }
      }else {
        $output .= "<tr>
                      <td colspan='5' style='text-align:center;color:gray;font-weight:500;'>No Data Found</td>
                    </tr>";
      }
      $output .="</table>";
      echo $output;
      }
    }
    public function Add_Product()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $data['categories'] = $this->cm->fetch_all_records('category','desc','limit');
        $this->load->view('Admin/upload_product',$data);
      }
    }
    public function Upload_Product()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $config = [
          'upload_path' => './uploads/product_image',
          'allowed_types' => 'jpg|jpeg|gif|png',
        ];
        $this->load->library('upload',$config);
        $this->upload->do_upload('product_image');
        $img = $this->upload->data('file_name');
        $data = [
          'product_title' => $this->input->post('product_title'),
          'image'         => $img,
          'category_id'   => $this->input->post('category_id'),
          'short_desc'    => $this->input->post('short_desc'),
          'color'         => $this->input->post('color'),
          'weight'        =>$this->input->post('weight'),
          'price'         =>$this->input->post('price'),
          'status'        =>"0",
          'upload_date'   =>date('Y-m-d'),
        ];
        if($data['product_title'] == "" && $data['image'] == "" && $data['price'] == "")
        {
          $this->session->set_flashdata('error','Please Enter Required Information');
        }else {
          $result = $this->cm->insert_data('product',$data);
          if($result == true){
            $this->session->set_flashdata('success','Congretulation ! Category Insert Successfully.');
          }else {
            $this->session->set_flashdata('fail','Fail ! Category Insert');
          }
        }
        return redirect('Admin/Add_Product');
      }
    }
    public function Change_Status($id,$status)
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $args = [
          'id'   =>  $id,
        ];
        $data = [
          'status'  =>$status
        ];
        $result = $this->cm->update_rec_by_args('category',$data,$args);
        if($result == true){
          $this->session->set_flashdata('success','Congretulation ! Category Status Update Successfully.');
        }else {
          $this->session->set_flashdata('error','Fail ! Category Status Update');
        }
        return redirect('Admin/Manage_Category');
      }
    }
    public function Change_Product_Status($id,$status)
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $args = [
          'id'   =>  $id,
        ];
        $data = [
          'status'  =>$status
        ];
        $result = $this->cm->update_rec_by_args('product',$data,$args);
        if($result == true){
          $this->session->set_flashdata('success','Congretulation ! Category Status Update Successfully.');
        }else {
          $this->session->set_flashdata('error','Fail ! Category Status Update');
        }
        return redirect('Admin/Manage_Product');
      }
    }
    public function Manage_Product()
    {
      $config['base_url'] = base_url('Admin/Manage_Product/');
      $config['per_page'] = 5;
      $config['total_rows'] = $this->db->count_all('product');

      $this->load->library('pagination',$config);
      $data['link'] = $this->pagination->create_links();
      $this->load->view('Admin/manage_product',$data);
    }
    public function Ajax_Product_Search($pagi)
    {

      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
      $offset = $pagi;
      echo $offset;
      $limit = 5;
      $filter = $this->input->post('filter');
      // Add Pagination
      $output = "";
      $query = "";
      if($this->input->post('query'))
      {
        $query = $this->input->post('query');
      }
      $data = $this->cm->ajax_product_search($query,$filter,$offset,$limit);
      $output .="
                  <table style='margin-top:0px;' class='table striped'>
                    <tr>
                      <th class='center'>Image</th>
                      <th style='padding-left:20px;'>Name</th>
                      <th>Category</th>
                      <th>Count Sold</th>
                      <th>Active</th>
                      <th style='padding-left:30px;'>Price</th>
                      <th class='center'>Action</th>
                    </tr>";
      if($data->num_rows()>0)
      {
        foreach($data->result() as $row)
        {
          $category_data = get_category_product_name($row->category_id);
          $output .="
          <tr>
            <td>
              <center>
                <img src='".base_url()."uploads/product_image/".$row->image."'class='responsive-img' id='category_image'>
              </center>
            </td>
            <td style='font-size:14px;color:gray;width:40%;padding-right:70px;padding-left:20px' title='".$row->product_title."'>".word_limiter($row->product_title,12)." <br> <a href='".base_url('Home/Product_Details/'.$row->id)."' style='font-size:13px;'>View On Home</a> </td>
            <td style='font-size:14px;color:gray;'><a href='".base_url('Home/Product_Categories/').$row->category_id."'style='color:gray;'href='#'>".$category_data[0]->category_name."</a></td>
            <td style='font-size:14px;color:gray;'><a style='color:gray;'href='#'>".$row->count_sale." Products</a></td>
            <td style='font-size:14px;color:gray;'>".($row->status == "0"? '<a href="'.base_url('Admin/Change_Product_Status/').$row->id.'/1'.'" style="color:gray;" title="Change Status"><span class="fa fa-toggle-on" style="font-size:13px;color:green;"></span> Active</a>'
            :'<a href="'.base_url('Admin/Change_Product_Status/').$row->id.'/0'.'"  title="Change Status" style="color:gray;"><span class="fas fa-toggle-off" style="font-size:13px;color:red;"></span> Inactive</a>')."</td>
            <td style='font-size:14px;color:gray;padding-left:30px;'><a style='color:gray;'href='#'><span class='fa fa-rupee-sign'> ".$row->price."</a></td>
            <td class='center'><a href='".base_url('Admin/Edit_Product/'.$row->id)."' style='color:gray;'title='Edit'><span class='fas fa-edit'></span></a> -
              <a href='".base_url('Admin/Delete_Product/'.$row->id)."'  style='color:gray;'title='Delete'> <span class='fa fa-trash-alt'></span> </a> </td>
          </tr>
          <ul class='dropdown-content action_dropdown' id='action_dropdown'>
          <li>ankit</li>
          </ul>";
        }
      }else {
        $output .= "<tr>
                      <td colspan='5' style='text-align:center;color:gray;font-weight:500;'>No Data Found</td>
                    </tr>";
      }
      $output .="</table>";
      echo $output;
      }
    }
    public function Edit_Product($id = "")
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $args = [
          'id' => $id
        ];
        $data['products'] = $this->cm->fetch_rec_by_args('product',$args);
        $data['categories'] = $this->cm->fetch_all_records('category','desc','limit');
        $this->load->view('Admin/edit_product',$data);
      }
    }
    public function Update_Product($id = "")
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $config = [
          'upload_path' => './uploads/product_image',
          'allowed_types' => 'jpg|jpeg|gif|png',
        ];
        $this->load->library('upload',$config);
        // check image
        if ($_FILES['product_image']['name'] == "") {

        }else {
          $args = [
            'id' =>$id,
          ];
          $old_data = $this->cm->fetch_rec_by_args('product',$args);
          // unlink old image
          unlink('uploads/product_image/'.$old_data[0]->image);
          // unlink old image
          $this->upload->do_upload('product_image');
          $img = $this->upload->data('file_name');
          $data['image'] = $img;
        }
        $data['product_title'] = $this->input->post('product_title');
        $data['category_id'] = $this->input->post('category_id');
        $data['short_desc'] = $this->input->post('short_desc');
        $data['color'] = $this->input->post('color');
        $data['weight'] = $this->input->post('weight');
        $data['price'] = $this->input->post('price');
        if ($data['product_title'] == "" && $data['category_id'] == "" && $data['price'] == "" ) {
          $this->session->set_flashdata('error','Please Enter Required Information.');
        }else {
          $args = [
            'id' => $id,
          ];
          $result = $this->cm->update_rec_by_args('product',$data,$args);
          if($result == true){
            $this->session->set_flashdata('success','Congretulation ! Product Update Successfully.');
          }else {
            $this->session->set_flashdata('fail','Fail ! Product Insert');
          }
        }
        return redirect("Admin/Edit_Product/".$id ,'refresh');
      }
    }
    public function Manage_Orders()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $this->load->view('Admin/manage_orders');
      }
    }
    public function Ajax_Order_Search()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
      $output = "";
      $query = "";
      if($this->input->post('query'))
      {
        $query = $this->input->post('query');
      }


      $data = $this->cm->ajax_order_search($query);
      $output .="
                  <table style='margin-top:0px;' class='table striped'>
                    <tr>
                      <th class='center'>Order ID</th>
                      <th>Customer Name</th>
                      <th>Quantity</th>
                      <th>Amount</th>
                      <th>Order Date</th>
                      <th>Status</th>
                    </tr>";
      if($data->num_rows()>0)
      {
        foreach($data->result() as $row)
        {
          $output .="
          <tr>
            <td class='center' style='font-size:14px;color:gray;'><a target='_blank' href='".base_url('Admin/Order_Details/'.$row->id)."'>#".$row->id."</a></td>
            <td style='font-size:14px;color:gray;padding-left:30px;font-weight:500;'>".$row->user_name."</td>
            <td style='font-size:14px;color:gray;'><span style='font-weight:500;'>".$row->total_quantity."</span></td>
            <td style='font-size:14px;color:gray;'><a style='color:gray;'href='#'><span class='fa fa-rupee-sign'></span> <span style='font-weight:500;'> ".number_format($row->total_amount)."</span></a></td>
            <td style='font-size:14px;color:gray;'><a style='color:gray;'href='#'><span style='font-weight:500;'>". date('d-M-Y',strtotime($row->order_date))."</span></a></td>
            <td style='font-size:14px;color:gray;font-weight:500;'>".($row->order_status == "Pending" ?'<span class="fa fa-circle" style="color:red;"></span>&nbsp;'.$row->order_status.'':'<span class="fa fa-circle" style="color:green;"></span>&nbsp;'.$row->order_status.'')."</td>
            </tr>";
        }
      }else {
        $output .= "<tr>
                      <td colspan='7' style='text-align:center;color:gray;font-weight:500;'>Order's Not Found..</td>
                    </tr>";
      }
      $output .="</table>";
      echo $output;
      }
    }
    public function Order_Details($id)
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $args = [
          'order_id'    =>$id
        ];
        $data['order_product'] = $this->cm->fetch_rec_by_args('order_product',$args);
        $args = [
          'id'          =>$id
        ];
        $data['uri'] = $this->uri->segment(4);
        $data['order']  = $this->cm->fetch_rec_by_args('orders',$args);
        $this->load->view('Admin/order_details',$data);
      }
    }
    public function Print_Slip($id)
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $args = [
          'order_id'    =>$id
        ];
        $data['order_product'] = $this->cm->fetch_rec_by_args('order_product',$args);
        $args = [
          'id'          =>$id
        ];
        $data['order']  = $this->cm->fetch_rec_by_args('orders',$args);
        $this->load->view('Admin/print_slip',$data);
      }
    }
    public function Change_Order_Status($id)
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        // count sold product start
        if($this->input->post('status') == "Delivered")
        {
          $args = [
            'order_id'    => $id,
          ];
          $check_product = $this->cm->fetch_rec_by_args('order_product',$args);
          if(count($check_product)){
            foreach($check_product as $cp)
            {
              $args = [
                'id'    => $cp->product_id
                ];
            $data = $this->cm->fetch_rec_by_args('product',$args);
            $count = $data[0]->count_sale + $cp->quantity;
            $result =   $this->db->where(['id'=>$cp->product_id])
                        ->update('product',['count_sale'=>$count]);
            }
          }else {

          }
        }else {

        }
        // count sold product end
        $args = [
          'id'          =>$id
        ];
        $data = [
          'order_status'    =>$this->input->post('status'),
          'delivered_date'  =>date('Y-m-d')
        ];
        $order  = $this->cm->update_rec_by_args('orders',$data,$args);
        if($order == true)
        {
          $this->session->set_flashdata('success','Successfully Change Order Status');
        }else {
          $this->session->set_flashdata('error','Fail ! Change Order Status');
        }
        return redirect('Admin/Order_Details/'.$id);
      }
    }
    public function Delivered_Orders()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $this->load->view('Admin/delivered_orders');
      }
    }
    public function Ajax_Delivered_Order()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
      $output = "";
      $query = "";
      if($this->input->post('query'))
      {
        $query = $this->input->post('query');
      }
      $data = $this->cm->ajax_delivered_order($query);
      $output .="
                  <table style='margin-top:0px;' class='table striped'>
                    <tr>
                      <th class='center'>Order ID</th>
                      <th>Customer Name</th>
                      <th>Quantity</th>
                      <th>Amount</th>
                      <th>Order Date</th>
                      <th>Status</th>
                    </tr>";
      if($data->num_rows()>0)
      {
        foreach($data->result() as $row)
        {
          $output .="
          <tr>
            <td class='center' style='font-size:14px;color:gray;'><a target='_blank' href='".base_url('Admin/Order_Details/'.$row->id.'/1')."'>#".$row->id."</a></td>
            <td style='font-size:14px;color:gray;padding-left:30px;font-weight:500;'>".$row->user_name."</td>
            <td style='font-size:14px;color:gray;'><span style='font-weight:500;'>".$row->total_quantity."</span></td>
            <td style='font-size:14px;color:gray;'><a style='color:gray;'href='#'><span class='fa fa-rupee-sign'></span> <span style='font-weight:500;'> ".number_format($row->total_amount)."</span></a></td>
            <td style='font-size:14px;color:gray;'><a style='color:gray;'href='#'><span style='font-weight:500;'>". date('d-M-Y',strtotime($row->order_date))."</span></a></td>
            <td style='font-size:14px;color:gray;font-weight:500;'>".($row->order_status == "Pending" ?'<span class="fa fa-circle" style="color:red;"></span>&nbsp;'.$row->order_status.'':'<span class="fa fa-circle" style="color:green;"></span>&nbsp;'.$row->order_status.'')."</td>
            </tr>";
        }
      }else {
        $output .= "<tr>
                      <td colspan='7' style='text-align:center;color:gray;font-weight:500;'>Order's Not Found..</td>
                    </tr>";
      }
      $output .="</table>";
      echo $output;
      }
    }
    public function All_Sales()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $args = [
          'order_status'   =>     'Delivered',
          'order_date>='     =>   date('Y-m-d',strtotime("-30 day")),
          'order_date<='      =>  date('Y-m-d'),
        ];
        $data['start_date'] = date('Y-m-d',strtotime('-30 Days'));
        $data['end_date'] = date('Y-m-d');
        $data['sales'] = $this->cm->fetch_all_sales($args);
        $this->load->view('Admin/all_sales',$data);
      }
    }
    public function Search_Sales()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        if($this->input->post('start_date') == "" && $this->input->post('end_date')=="")
        {
          return redirect('Admin/All_Sales');
        }
        $start_date ="";
        $args = [
        'order_status'   =>     'Delivered',
        'order_date>='   =>     $this->input->post('start_date'),
        'order_date<='   =>     $this->input->post('end_date')
      ];
        $data['start_date'] = $this->input->post('start_date');
        $data['end_date'] = $this->input->post('end_date');
        $data['sales'] = $this->cm->fetch_all_sales($args);
        $this->load->view('Admin/all_sales',$data);
      }
    }
    public function Today_Sales()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $args = [
        'order_status'   =>     'Delivered',
        'order_date'   =>       date('Y-m-d'),
      ];
        $data['start_date'] = date('Y-m-d',strtotime('-30 Days'));
        $data['end_date'] = date('Y-m-d');
        $data['sales'] = $this->cm->fetch_all_sales($args);
        $this->load->view('Admin/all_sales',$data);
      }
    }
    public function count_order($type)
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        if($type ==  "all")
        {
          $orders = $this->cm->fetch_all_records('orders','desc','limit');
        }elseif($type == "today")
        {
          $args = [
            'order_date'  => date('Y-m-d')
          ];
          $orders = $this->cm->fetch_rec_by_args('orders',$args);
        }elseif($type == "yesterday")
        {
          $args = [
            'order_date'  => date('Y-m-d',strtotime("-1 day"))
          ];
          $orders = $this->cm->fetch_rec_by_args('orders',$args);
        }elseif($type == "last_30")
        {
          $args = [
            'order_date>='  => date('Y-m-d',strtotime("-30 day")),
          ];
          $orders = $this->cm->fetch_rec_by_args('orders',$args);
        } else {
          $orders = $this->cm->fetch_all_records('orders','desc','limit');
        }
        echo count($orders);
      }
    }
    public function Pending_Orders()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $this->load->view('Admin/pending_orders');
      }
    }
    public function Ajax_Pending_Orders()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
      $output = "";
      $query = "";
      if($this->input->post('query'))
      {
        $query = $this->input->post('query');
      }
      $data = $this->cm->ajax_pending_order($query);
      $output .="
                  <table style='margin-top:0px;' class='table striped'>
                    <tr>
                      <th class='center'>Order ID</th>
                      <th>Customer Name</th>
                      <th>Quantity</th>
                      <th>Amount</th>
                      <th>Order Date</th>
                      <th>Status</th>
                    </tr>";
      if($data->num_rows()>0)
      {
        foreach($data->result() as $row)
        {
          $output .="
          <tr>
            <td class='center' style='font-size:14px;color:gray;'><a target='_blank' href='".base_url('Admin/Order_Details/'.$row->id)."'>#".$row->id."</a></td>
            <td style='font-size:14px;color:gray;padding-left:30px;font-weight:500;'>".$row->user_name."</td>
            <td style='font-size:14px;color:gray;'><span style='font-weight:500;'>".$row->total_quantity."</span></td>
            <td style='font-size:14px;color:gray;'><a style='color:gray;'href='#'><span class='fa fa-rupee-sign'></span> <span style='font-weight:500;'> ".number_format($row->total_amount)."</span></a></td>
            <td style='font-size:14px;color:gray;'><a style='color:gray;'href='#'><span style='font-weight:500;'>". date('d-M-Y',strtotime($row->order_date))."</span></a></td>
            <td style='font-size:14px;color:gray;font-weight:500;'>".($row->order_status == "Pending" ?'<span class="fa fa-circle" style="color:red;"></span>&nbsp;'.$row->order_status.'':'<span class="fa fa-circle" style="color:green;"></span>&nbsp;'.$row->order_status.'')."</td>
            </tr>";
        }
      }else {
        $output .= "<tr>
                      <td colspan='7' style='text-align:center;color:gray;font-weight:500;'>Order's Not Found..</td>
                    </tr>";
      }
      $output .="</table>";
      echo $output;
      }
    }
    public function Customers()
    {
      if($this->session->userdata('admin_id') == "")
      {
        return redirect('Admin/index');
      }else {
        $data['customer'] = $this->cm->view_all_customer();
        $this->load->view('Admin/customer_details',$data);
      }
    }
  }
?>
