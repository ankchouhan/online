<?php
  /**
   *
   */
  class Home extends CI_Controller
  {
    private $user_profile;
    public function __construct()
    {
      parent :: __construct();
      $this->load->model('Main',"cm");

      // user details
      $args = [
        'email'   => $this->session->userdata('email'),
        'password'   => $this->session->userdata('password'),
      ];
      $this->user_profile = $this->cm->fetch_rec_by_args('users',$args);
    }
    public function index()
    {
      $args = [
        'status' =>'0',
      ];
      $data["categories"] = $this->cm->fetch_rec_by_args_rand('category',$args);
      $this->load->view('Home/index',$data);
    }
    public function Dashboard()
    {
      if($this->session->userdata('email') == "" && $this->session->userdata('password') == "")
      {
        return redirect('Home/User_Signin');
      }
      else {
        $user = $this->user_profile;
          $args=[
            'id' =>$user[0]->id,
          ];
      $data['user'] = $this->cm->fetch_rec_by_args('users',$args);
        $args=[
          'user_id' =>$user[0]->id,
        ];
      $data['orders'] = $this->cm->fetch_rec_by_args('orders',$args);
      $args = [
        'session_id'  =>$this->session->userdata('session_id')
      ];
        $data['carts'] = $this->cm->fetch_rec_by_args('cart',$args);
      $args=[
        'user_id' =>$user[0]->id,
        'order_status'=>'Delivered',
      ];
        $data['delivered'] = $this->cm->fetch_rec_by_args('orders',$args);
      $this->load->view('Home/dashboard',$data);
      }
    }
    public function User_Signup($page ="")
    {
      if($this->session->userdata('email') == ""  &&  $this->session->userdata('password') == "")
      {
        $this->load->view('Home/user_signup',['page'=>$page]);
      }else {
        return redirect('Home/dashboard');
      }
    }
    public function User_Register($page ="")
    {
      $data = [
        'fullname'  =>  $this->input->post('full_name'),
        'email'     =>  $this->input->post('email'),
        'mobile'    =>  $this->input->post('mobile_no'),
        'password'  =>  $this->input->post('password'),
        'address'   =>  $this->input->post('address'),
        'register_date' => date('Y-m-d'),
      ];
      if($data['fullname'] == "" && $data['email'] == "" && $data['password'] == ""){
        $this->session->set_flashdata('error','Please Enter Required Information');
      }else {
        $result = $this->cm->insert_data('users',$data);
        if($result == true )
        {
          $user_session = [
            'email' => $data['email'],
            'password' => $data['password']
          ];
          $this->session->set_userdata($user_session);
          if($page == "cart")
          {
            return redirect('Home/Carts');
          }else {
            return redirect('Home/Dashboard');
          }
        }else {
          $this->session->set_flashdata('error','User Registration Fail.');
        }
      }
      return redirect('Home/User_Signup'.$page);
    }
    public function User_Signin($page = "")
    {
      if($this->session->userdata('email') == ""  &&  $this->session->userdata('password') == "")
      {
        $this->load->view('Home/user_signin',['page'=>$page]);
      }else {
        return redirect('Home/dashboard');
      }
    }
    public function User_Login($page =""){
       $args = [
         'email'        =>$this->input->post('email'),
         'password'     =>$this->input->post('password')
       ];
       $result = $this->cm->fetch_rec_by_args('users',$args);
       if($result == true)
       {
           $user_session = [
             'email' => $args['email'],
             'password' => $args['password']
           ];
           $this->session->set_userdata($user_session);
           if($page == "cart")
           {
             return redirect('Home/Carts');
           }else {
             return redirect('Home/Dashboard');
           }
       }else {
         $this->session->set_flashdata('error','Your Username & Password is Incorrect');
         return redirect('Home/User_Signin');
       }
     }
     public function User_Logout()
     {
       $this->session->unset_userdata('email');
       $this->session->unset_userdata('password');
       return redirect('Home/User_Signin');
     }
     public function My_Order($page ="")
     {
       if($this->session->userdata('email') == ""  &&  $this->session->userdata('password') == "")
       {
         $this->load->view('Home/user_signin',['page'=>$page]);
       }else {
       $user = $this->user_profile;
       $args = [
         'user_id' => $user[0]->id
       ];
       $data['orders'] = $this->cm->fetch_rec_by_args('orders',$args);
       $this->load->view('Home/my_order',$data);
     }
   }
    public function Carts()
    {
      $args = [
        'session_id' => $this->session->userdata('session_id')
      ];
      $data['carts'] = $this->cm->fetch_rec_by_args('cart',$args);
      $this->load->view('Home/my_cart',$data);
    }
    public function Product_Categories($id = "")
    {
      if($id == "")
      {
        return redirect('Home/index');
      }else{
      $args = [
        'category_id' => $id,
        'status' =>"0"
      ];
      $data['products'] = $this->cm->fetch_rec_by_args('product',$args);
      $args = [
        'id'  => $id,
      ];
      $data['category_details'] = $this->cm->fetch_rec_by_args('category',$args);
      $this->load->view('Home/product_categories',$data);
    }
  }
    public function Product_Filter($id,$order)
    {
      if($order == "default"){
        $order_format = [
          'column_name' => 'id',
          'order'       =>'desc'
        ];
      }
        elseif ($order == "best_match") {
          $order_format = [
            'column_name' => 'count_sale',
            'order'       =>'desc'
          ];
        }elseif ($order == "lowest_price") {
          $order_format = [
            'column_name' => 'price',
            'order'       =>'asc'
          ];
        }elseif ($order == "highest_price") {
          $order_format = [
            'column_name' => 'price',
            'order'       =>'desc'
          ];
        }else {
          $order_format = [
            'column_name' => 'id',
            'order'       =>'desc'
          ];
        }
        $args = [
          'category_id' => $id
        ];
        $data['products'] = $this->cm->fetch_rec_by_arg_with_order('product',$args,$order_format);
        $args =[
          'id' =>$id
        ];
        $data['category_details'] = $this->cm->fetch_rec_by_args('category',$args);
        $this->load->view('Home/product_categories',$data);
    }
    public function Product_Details($id = "")
    {
      if($id == "")
      {
        return redirect("Home/index");
      }else{
      $args = [
        'id' =>$id
      ];
      $data['product'] = $this->cm->fetch_rec_by_args('product',$args);
      $args = [
        'id!=' => $id,
        'category_id' => $data['product'][0]->category_id,
      ];
      $data['releated_product'] = $this->cm->fetch_rec_by_args_with_limit('product',$args,'6');
      $this->load->view('Home/product_Details',$data);
    }
  }
    public function product_detail_modal($id)
    {
      $args = [
        'id' =>$id
      ];
      $data['product'] = $this->cm->fetch_rec_by_args('product',$args);
      $this->load->view('Home/product_details_modal',$data);
    }
    public function add_to_cart($id)
    {
      if($this->session->userdata('session_id') == ""){
        $user_session_id = [
          'session_id' =>rand(9999,999999)
        ];
        $this->session->set_userdata($user_session_id);
      }else {
        // $session_id = $this->session->userdata('session_id');
      }
      $args = [
        'id' =>$id
      ];
      $product_details = $this->cm->fetch_rec_by_args('product',$args);
      $args = [
        'product_id'    =>$id,
        'session_id'    =>$this->session->userdata('session_id'),
      ];
      $check_product = $this->cm->fetch_rec_by_args('cart',$args);
      if(count($check_product)){
        $old_qty = $check_product[0]->quantity;
        $new_qty = $old_qty +1;
        $args = [
            'id'    =>$check_product[0]->id,
        ];
        $data = [
            'quantity' =>$new_qty,
        ];
        $result = $this->cm->update_rec_by_args('cart',$data,$args);
        if($result == true)
        {
          echo "1";
        }else {
          echo "0";
        }
      }else {
        $data =[
          'product_id'    =>  $product_details[0]->id,
          'session_id'    =>  $this->session->userdata('session_id'),
          'product_name'  =>  $product_details[0]->product_title,
          'quantity'      =>  '1',
          'rate'          =>$product_details[0]->price,
        ];
        $result = $this->cm->insert_data('cart',$data);
        if($result == true)
        {
          echo '1';
        }else {
          echo '0';
        }
      }
    }
    public function Remove_Product($id)
    {
      $args = [
        'id' => $id,
        'session_id' => $this->session->userdata('session_id')
      ];
      $result = $this->cm->delete_rec_by_args('cart',$args);
      if($result == true)
      {
        $this->session->set_flashdata('success','Remove Product Successfully.');
      }else {
        $this->session->set_flashdata('error','Remove Product Fail.');
      }
      return redirect('Home/Carts');
    }
    public function Update_Quantity($quantity,$type,$product_id)
    {
      if($type == "add")
      {
        $new_qty = $quantity + 1;
        $args = [
          'product_id' => $product_id
        ];
        $data = [
          'quantity' => $new_qty
        ];
        $result = $this->cm->update_rec_by_args('cart',$data,$args);
      }else {
          if($quantity > 1){
            $new_qty = $quantity - 1;
            $args = [
              'product_id' => $product_id
            ];
            $data = [
              'quantity' =>$new_qty
            ];
            $result = $this->cm->update_rec_by_args('cart',$data,$args);
        }else {
          $result = false;
        }
      }
      if($result == true)
      {
        echo "1";
      }else {
        echo "0";
      }
    }
    public function calculate_cart_products()
    {
      $args = [
        'session_id' => $this->session->userdata('session_id'),
      ];
      $products = $this->cm->fetch_rec_by_args('cart',$args);
      $cal_amount ="0";
      if(count($products))
      {
        foreach ($products as $product) {
        $cal_amount += ($product->rate * $product->quantity);
        }
      }else {
        $cal_amount = "0";
      }
      $data = [
        'total_product' =>  count($products),
        'total_amount'  =>  ($cal_amount > 0) ? number_format($cal_amount):"0",
      ];
      echo json_encode($data);
    }
    public function Place_Order()
    {
      $args = [
        'session_id'  =>  $this->session->userdata('session_id')
      ];
      $data['products'] = $this->cm->fetch_rec_by_args('cart',$args);
      $this->load->view('Home/Place_Order',$data);
    }
    public function Save_Temp_Address($user_id)
    {
      if($this->session->userdata('email') == "" && $this->session->userdata('password') == "")
      {
        return redirect('Home/User_Signin');
      }
      else {
        $data= [
          'user_id'        =>  $user_id,
          'address'         =>  $this->input->post('shipping_address'),
        ];
        if($data['id'] == "" && $data['address'] =="")
        {
          $this->session->set_flashdata('error', 'Please Enter Shipping Address.');
          return redirect('Home/Place_Order');
        }else {
          $result = $this->cm->insert_data('temp_address',$data);
          if($result == true)
          {
            $this->session->set_flashdata('success','Save Shipping Address Successfully.  ');
            return redirect('Home/Place_Order');
          }else {
            $this->session->set_flashdata('error','Fail ! Save Shipping Address');
            return redirect('Home/Place_Order');
          }
        }
      }
    }
    public function Complete_Purchased()
    {
      if($this->session->userdata('email') == "" && $this->session->userdata('password') == "")
      {
        return redirect('Home/User_Signin');
      }
      else {
        $args = [
          'session_id'  =>  $this->session->userdata('session_id')
        ];
        $products = $this->cm->fetch_rec_by_args('cart',$args);
        if(count($products)>0)
        {
        $user = $this->user_profile;
        // get shipping address start
        $args = [
          'user_id'  =>$user[0]->id
        ];
        $temp_address = $this->cm->fetch_rec_by_args('temp_address',$args);
        // get shipping address end
        $total_quantity = 0;
        $total_amount = 0;
        if(count($products))
        {
          foreach ($products as $pro) {
            $total_quantity += $pro->quantity;
            $total_amount += ($pro->quantity * $pro->rate);
          }
        }else {
          $total_quantity = 0;
        }
        $data = [
          'user_id'           =>  $user[0]->id,
          'user_name'         =>  $user[0]->fullname,
          'total_quantity'    =>  $total_quantity,
          'total_amount'      =>  $total_amount,
          'order_date'        =>  date('Y-m-d'),
          'shipping_address'  =>  $temp_address[0]->address == "" ? $user[0]->address:$temp_address[0]->address,
          'order_status'      =>  'Pending',
        ];
        $order_id = $this->cm->insert_data_with_last_id('orders',$data);
        $args = [
          'id'      =>  $user[0]->id,
        ];
        $user_add = $this->cm->fetch_rec_by_args('users',$args);
        $data =[
          'user_id'       =>  $user[0]->id,
          'address'       =>   $user_add[0]->address,
        ];
        $result = $this->cm->insert_data('temp_address',$data);

        // //insert order products
        if(count($products))
        {
          foreach ($products as $pro) {
            $result = $this->db->insert('order_product',[
          'order_id'      =>  $order_id,
          'product_id'    =>  $pro->product_id,
          'product_name'  =>  $pro->product_name,
          'quantity'      =>  $pro->quantity,
          'rate'          =>  $pro->rate,

        ]);
          }
        }else {

        }
        //insert order products
        $args = [
          'session_id'    =>$this->session->userdata('session_id'),
        ];
        $result  = $this->cm->delete_rec_by_args('cart',$args);

        $args = [
          'user_id'       => $user[0]->id
        ];
        $result = $this->cm->delete_rec_by_args('temp_address',$args);
        if($result == true)
        {
          $this->session->set_flashdata('success',"Congretulation Your Product Placed Successfully.");
        }else {
          $this->session->set_flashdata('error','Fail ! To Your Product Placed');
        }
        return redirect("Home/Dashboard");
        }
        else{
          $this->session->set_flashdata('error',"No products in Cart");
          return redirect('Home/index');
        }
      }
    }
    public function Buy_Product($id)
    {
      if($this->session->userdata('session_id') == ""){
        $user_session_id = [
          'session_id' =>rand(9999,999999)
        ];
        $this->session->set_userdata($user_session_id);
      }else {
        // $session_id = $this->session->userdata('session_id');
      }
      $args = [
        'id' =>$id
      ];
      $product_details = $this->cm->fetch_rec_by_args('product',$args);
      $args = [
        'product_id'    =>$id,
        'session_id'    =>$this->session->userdata('session_id'),
      ];
      $check_product = $this->cm->fetch_rec_by_args('cart',$args);
      if(count($check_product)){
        $old_qty = $check_product[0]->quantity;
        $new_qty = $old_qty +1;
        $args = [
            'id'    =>$check_product[0]->id,
        ];
        $data = [
            'quantity' =>$new_qty,
        ];
        $result = $this->cm->update_rec_by_args('cart',$data,$args);
        if($result == true)
        {
          echo "1";
        }else {
          echo "0";
        }
      }else {
        $data =[
          'product_id'    =>  $product_details[0]->id,
          'session_id'    =>  $this->session->userdata('session_id'),
          'product_name'  =>  $product_details[0]->product_title,
          'quantity'      =>  '1',
          'rate'          =>$product_details[0]->price,
        ];
        $result = $this->cm->insert_data('cart',$data);
        if($result == true)
        {
          echo '1';
        }else {
          echo '0';
        }
      }
      return redirect('Home/Place_Order');
    }
    public function Search_Products()
    {
      $val = $this->input->post('search_box');
      $args = [
        'product_title' =>$val
      ];
      $product = $this->cm->fetch_rec_by_args_with_like('product',$args);
      $output ='';
      if(count($product)){
        $i=0;
        foreach($product as $pro)
        {
          $i++;
          $output.='<li><a target="_blank "href="'.base_url('Home/product_Details/').$pro->id.'">'.word_limiter($pro->product_title,5).'<span class="right" style="margin-right:5px;"><span class="fa fa-link"> </span></span> </a></li>';
          if($i>9):break;

        endif;
        }
      }else {
        $output .='<li><a href="">Product Not Found.</a></li>';
      }
      echo $output;
    }
    public function Shipping_policy()
    {
      $this->load->view('Home/shipping_policy');
    }
    public function Delivered_Orders($page="")
    {
        if($this->session->userdata('email') == ""  &&  $this->session->userdata('password') == "")
        {
          $this->load->view('Home/user_signin',['page'=>$page]);
        }else {
        $user = $this->user_profile;
        $args = [
          'user_id' => $user[0]->id,
          'order_status' => "Delivered"
        ];
        $data['orders'] = $this->cm->fetch_rec_by_args('orders',$args);
        $this->load->view('Home/delivered_orders',$data);
      }
    }
    public function Manage_Profile($page ="")
    {
      if($this->session->userdata('email') == ""  &&  $this->session->userdata('password') == "")
      {
        $this->load->view('Home/user_signin',['page'=>$page]);
      }else {
        $user =$this->user_profile;
        $args = [
          'id'    =>$user[0]->id
        ];
        $data = $this->cm->fetch_rec_by_args('users',$args);
        $this->load->view('Home/manage_profile',['page'=>$page,'user'=>$data]);
      }
    }
    public function Update_Profile($page="")
    {
      if($this->session->userdata('email') == ""  &&  $this->session->userdata('password') == "")
      {
        $this->load->view('Home/user_signin',['page'=>$page]);
      }else {
        $user =$this->user_profile;
        $args = [
          'id'    =>$user[0]->id
        ];
      $data = [
        'fullname'  =>  $this->input->post('full_name'),
        'mobile'    =>  $this->input->post('mobile_no'),
        'password'  =>  $this->input->post('password'),
        'address'   =>  $this->input->post('address'),
      ];
      if($data['fullname'] == "" && $data['email'] == "" && $data['password'] == ""){
        $this->session->set_flashdata('error','Please Enter Required Information');
      }else {
        $result = $this->cm->update_rec_by_args('users',$data,$args);
        if($result == true )
        {
            $this->session->set_flashdata('success',"Update Information Successfully..");
            return redirect('Home/Dashboard');
        }else {
          $this->session->set_flashdata('error','Update Information Fail.');
        }
      return redirect('Home/Manage_Profile'.$page);
      }
    }
  }
    public function Forget_password()
    {
      $this->session->unset_userdata('email');
      $this->load->view('Home/Forget_password');
    }
    public function Confirm_Email()
    {
      $args = [
        'email'     => $this->input->post('email')
      ];
      $user_session = [
        'email' => $args['email'],
      ];
      $this->session->set_userdata($user_session);
      $result =  $this->cm->fetch_rec_by_args('users',$args);
      if($result == true)
      {
      $data = [
        'password'    => $result[0]->password
      ];
    }else {
      $data = [
        'password' => "0"
      ];
    }
      echo json_encode($data);
    }
    public function change_password()
    {
      if($this->session->userdata('email')== "")
      {
        return redirect('Home/Forget_password');
      }
      else {
        $args = [
          'email' =>$this->session->userdata('email')
        ];
        print_r($args);
        $data = [
          'password'  =>$this->input->post('password')
        ];
        $result = $this->cm->update_rec_by_args('users',$data,$args);
        if($result == true)
        {
          $this->session->set_flashdata('success',"Change Password Successfully..");
          $this->session->unset_userdata('email');
          return redirect("Home/User_Signin",$page);
        }else {
          $this->session->set_flashdata('error',"Fail ! Change Password Process.");
          $this->session->unset_userdata('email');
          return redirect("Home/User_Signin",$page);
        }
      }
    }
  }
?>
