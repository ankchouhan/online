-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2020 at 07:51 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `login_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `fullname`, `login_date`) VALUES
(1, 'Admin', 'admin', 'ankit', '16/9/2020');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` decimal(11,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `product_id`, `session_id`, `product_name`, `quantity`, `rate`) VALUES
(33, 8, 47583, 'Backpack for Men and Women|Unisex Backpack|College Bag for Boys and Girls|office Backpack |School Bag|Trendy Backpack|Stylish Backpack 30 L Laptop Backpack  (Red)', 1, '531'),
(34, 7, 47583, 'Vivo Y12 (Burgundy Red, 64 GB)  (3 GB RAM)', 1, '9900'),
(35, 13, 47583, 'PLAYER BACKPACK 01 - TEAL 28 L Backpack  (Blue, Green, White)', 3, '649'),
(36, 7, 662164, 'Vivo Y12 (Burgundy Red, 64 GB)  (3 GB RAM)', 1, '9900'),
(38, 15, 662164, 'OPPO Enco W31 ETI11 / ETI112 Bluetooth Headset  (Black, True Wireless)', 1, '3500'),
(39, 6, 662164, 'New Inspiron 15 5590 Laptop', 3, '63989'),
(50, 14, 669657, 'Adventure Stylish Series Water Resistance Trekking Hiking Travel Bag Rucksack - 55 L  (Orange, Black', 1, '764');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `cat_image` varchar(255) NOT NULL,
  `products` int(255) NOT NULL,
  `status` int(1) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `cat_image`, `products`, `status`, `date`) VALUES
(54, 'Laptop', 'lappp.jpg', 2, 0, '2020-09-18'),
(55, 'Headphone', 'jb.jpg', 10, 0, '2020-09-18'),
(56, 'Video Game', 'download_(1).jpg', 5, 0, '2020-09-19'),
(59, 'Goggel', 'download_(3).jpg', 17, 0, '2020-09-19'),
(60, 'Bags', 'images_(2).jpg', 29, 0, '2020-09-19'),
(61, 'Mobile', 'download_(2).jpg', 55, 0, '2020-09-21'),
(62, 'Footwear', 'download.jpg', 0, 0, '2020-09-28');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `total_quantity` decimal(11,0) NOT NULL,
  `total_amount` decimal(11,0) NOT NULL,
  `order_date` date NOT NULL,
  `shipping_address` varchar(255) NOT NULL,
  `order_status` varchar(255) NOT NULL,
  `delivered_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `user_name`, `total_quantity`, `total_amount`, `order_date`, `shipping_address`, `order_status`, `delivered_date`) VALUES
(28, 2, 'Vinay', '1', '764', '2020-07-23', '59, Rishi Nagar Ujjain', 'Pending', '0000-00-00'),
(31, 1, 'Ankit', '1', '999', '2020-09-23', '12, Namdarpura Ujjain', 'Delivered', '2020-09-25'),
(37, 2, 'yash', '3', '2255', '2020-08-17', '59, Rishi Nagar Ujjain', 'Delivered', '2020-08-20'),
(40, 3, 'Deepak', '1', '193', '2020-09-24', 'Indore', 'Packed', '0000-00-00'),
(43, 3, 'Deepak Chouhan', '1', '764', '2020-09-26', 'Bhawarkua Indore 456001', 'Delivered', '2020-09-28'),
(44, 2, 'Vinay Soni', '1', '63989', '2020-09-27', '59, Nanda Nagar Ujjain 456001', 'Pending', '0000-00-00'),
(46, 1, 'Ankit Chouhan', '1', '9900', '2020-09-27', '12, Namdarpura Ujjain', 'Pending', '0000-00-00'),
(47, 1, 'Ankit Chouhan', '3', '11080', '2020-09-29', '12, Namdarpura Ujjain', 'Pending', '0000-00-00'),
(48, 1, 'Ankit Chouhan', '1', '63989', '2020-09-29', '12, Namdarpura Ujjain', 'Delivered', '2020-09-30'),
(49, 2, 'Vinay Soni', '2', '127978', '2020-09-30', '59, Rishi Nagar Ujjain 456001', 'Pending', '0000-00-00'),
(50, 7, 'Anshul Jain', '1', '531', '2020-09-30', 'Vijay nagar Indore 456225', 'Pending', '0000-00-00'),
(51, 7, 'Anshul Jain', '1', '10200', '2020-09-30', 'Vijay nagar Indore 456225', 'Pending', '2020-09-30');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `quantity` decimal(11,0) NOT NULL,
  `rate` decimal(11,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `product_name`, `quantity`, `rate`) VALUES
(15, 27, 9, 'POCO M2 (Slate Blue, 64 GB)  (6 GB RAM)', '2', '10200'),
(16, 27, 7, 'Vivo Y12 (Burgundy Red, 64 GB)  (3 GB RAM)', '1', '9900'),
(17, 28, 14, 'Adventure Stylish Series Water Resistance Trekking Hiking Travel Bag Rucksack - 55 L  (Orange, Black', '1', '764'),
(18, 29, 17, 'HP 15s eq0007au 15.6-inch Laptop (3rd Gen Ryzen 3 3200U/4GB/256GB SSD/Windows 10/MS Office 2019/Radeon Vega 3 Graphics), Natural Silver', '1', '33434'),
(19, 29, 15, 'OPPO Enco W31 ETI11 / ETI112 Bluetooth Headset  (Black, True Wireless)', '2', '3500'),
(20, 30, 8, 'Backpack for Men and Women|Unisex Backpack|College Bag for Boys and Girls|office Backpack |School Bag|Trendy Backpack|Stylish Backpack 30 L Laptop Backpack  (Red)', '1', '531'),
(21, 30, 12, ' Round Sunglasses (55)  (Blue, Silver)', '1', '193'),
(22, 31, 18, 'Hitman Trilogy (Masterpiece) for pc', '1', '999'),
(23, 32, 16, 'Mivi DuoPods M20 True Wireless Bluetooth Headset  (White, True Wireless)', '1', '1899'),
(24, 35, 17, 'HP 15s eq0007au 15.6-inch Laptop (3rd Gen Ryzen 3 3200U/4GB/256GB SSD/Windows 10/MS Office 2019/Radeon Vega 3 Graphics), Natural Silver', '1', '33434'),
(25, 36, 7, 'Vivo Y12 (Burgundy Red, 64 GB)  (3 GB RAM)', '1', '9900'),
(26, 37, 12, ' Round Sunglasses (55)  (Blue, Silver)', '1', '193'),
(27, 37, 14, 'Adventure Stylish Series Water Resistance Trekking Hiking Travel Bag Rucksack - 55 L  (Orange, Black', '1', '764'),
(28, 37, 13, 'PLAYER BACKPACK 01 - TEAL 28 L Backpack  (Blue, Green, White)', '2', '649'),
(29, 38, 6, 'New Inspiron 15 5590 Laptop', '1', '63989'),
(30, 38, 16, 'Mivi DuoPods M20 True Wireless Bluetooth Headset  (White, True Wireless)', '2', '1899'),
(31, 39, 5, 'JBL T450 Wired Headphone, Blue', '1', '1155'),
(32, 40, 12, ' Round Sunglasses (55)  (Blue, Silver)', '1', '193'),
(33, 41, 17, 'HP 15s eq0007au 15.6-inch Laptop (3rd Gen Ryzen 3 3200U/4GB/256GB SSD/Windows 10/MS Office 2019/Radeon Vega 3 Graphics), Natural Silver', '1', '33434'),
(34, 42, 12, ' Round Sunglasses (55)  (Blue, Silver)', '1', '193'),
(35, 42, 7, 'Vivo Y12 (Burgundy Red, 64 GB)  (3 GB RAM)', '1', '9900'),
(36, 43, 14, 'Adventure Stylish Series Water Resistance Trekking Hiking Travel Bag Rucksack - 55 L  (Orange, Black', '1', '764'),
(37, 44, 6, 'New Inspiron 15 5590 Laptop', '1', '63989'),
(38, 45, 7, 'Vivo Y12 (Burgundy Red, 64 GB)  (3 GB RAM)', '1', '9900'),
(39, 46, 7, 'Vivo Y12 (Burgundy Red, 64 GB)  (3 GB RAM)', '1', '9900'),
(40, 47, 8, 'Backpack for Men and Women|Unisex Backpack|College Bag for Boys and Girls|office Backpack |School Bag|Trendy Backpack|Stylish Backpack 30 L Laptop Backpack  (Red)', '1', '531'),
(41, 47, 13, 'PLAYER BACKPACK 01 - TEAL 28 L Backpack  (Blue, Green, White)', '1', '649'),
(42, 47, 7, 'Vivo Y12 (Burgundy Red, 64 GB)  (3 GB RAM)', '1', '9900'),
(43, 48, 6, 'New Inspiron 15 5590 Laptop', '1', '63989'),
(44, 49, 6, 'New Inspiron 15 5590 Laptop', '2', '63989'),
(45, 50, 8, 'Backpack for Men and Women|Unisex Backpack|College Bag for Boys and Girls|office Backpack |School Bag|Trendy Backpack|Stylish Backpack 30 L Laptop Backpack  (Red)', '1', '531'),
(46, 51, 9, 'POCO M2 (Slate Blue, 64 GB)  (6 GB RAM)', '1', '10200');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `short_desc` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `weight` varchar(255) NOT NULL,
  `price` int(255) NOT NULL,
  `status` int(1) NOT NULL,
  `upload_date` date NOT NULL,
  `count_sale` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_title`, `category_id`, `image`, `short_desc`, `color`, `weight`, `price`, `status`, `upload_date`, `count_sale`) VALUES
(5, 'JBL T450 Wired Headphone, Blue', 55, 'jb.jpg', 'Durable & tangle free smart cable\r\n1-button remote with microphone\r\nLightweight & foldable design', 'Blue', '450g', 1155, 0, '2020-09-19', 2),
(6, 'New Inspiron 15 5590 Laptop', 54, 'notebook-inspiron-15-5590-silver-campaign-hero-504x350-ng.jpg', '10th Generation Intel® Core™ i5-10210U Processor (6MB Cache, up to 4.2 GHz)\r\nWindows 10 Home Single Language, English\r\nIntel® UHD Graphics with shared graphics memory\r\n8GB,4Gx1 + 4G onboard, DDR4, 2666MHz\r\n512GB M.2 PCIe NVMe Solid State Drive\r\nLCD Back C', 'Silver', '950g', 63989, 0, '2020-09-19', 7),
(7, 'Vivo Y12 (Burgundy Red, 64 GB)  (3 GB RAM)', 61, 'vivo.jpeg', '3 GB RAM | 64 GB ROM | Expandable Upto 256 GB\r\n16.13 cm (6.35 inch) HD+ Display\r\n13MP + 2MP + 8MP | 8MP Front Camera\r\n5000 mAh Battery\r\nMediaTek Helio P22 Processor', 'Aqua Blue', '320g', 9900, 0, '2020-09-19', 19),
(8, 'Backpack for Men and Women|Unisex Backpack|College Bag for Boys and Girls|office Backpack |School Bag|Trendy Backpack|Stylish Backpack 30 L Laptop Backpack  (Red)', 60, 'bag.jpeg', '|School Bag|Trendy Backpack|Stylish Backpack 30 L Laptop Backpack  (Red)', 'Red', '120g', 531, 0, '2020-09-19', 20),
(9, 'POCO M2 (Slate Blue, 64 GB)  (6 GB RAM)', 61, 'poco.jpeg', 'POCO M2 (Slate Blue, 64 GB) ,(6 GB RAM)', 'Pitch Black', '400g', 10200, 0, '2020-09-19', 9),
(12, ' Round Sunglasses (55)  (Blue, Silver)', 59, 'rou.jpeg', 'UV Protection', 'Blue, Silver', '', 193, 0, '2020-09-19', 4),
(13, 'PLAYER BACKPACK 01 - TEAL 28 L Backpack  (Blue, Green, White)', 60, 'gg.jpeg', 'This is a genuine American Tourister product. The product comes with a standard brand warranty of 1 Year.', 'Blue, Green, White', '', 649, 0, '2020-09-19', 6),
(14, 'Adventure Stylish Series Water Resistance Trekking Hiking Travel Bag Rucksack - 55 L  (Orange, Black', 60, 'aa.jpeg', 'n/a', 'Orange, Black', '', 764, 0, '2020-09-19', 5),
(15, 'OPPO Enco W31 ETI11 / ETI112 Bluetooth Headset  (Black, True Wireless)', 55, 'oppo.jpeg', 'OPPO Enco W31 ETI11 / ETI112 Bluetooth Headset  (Black, True Wireless)', 'Black, Wireless', '150g', 3500, 0, '2020-09-19', 2),
(16, 'Mivi DuoPods M20 True Wireless Bluetooth Headset  (White, True Wireless)', 55, 'mivi.jpeg', 'With Mic:Yes\r\nBluetooth version: 5.0\r\nWireless range: 10 m\r\nBattery life: 4 hr', 'White', '', 1899, 0, '2020-09-19', 47),
(17, 'HP 15s eq0007au 15.6-inch Laptop (3rd Gen Ryzen 3 3200U/4GB/256GB SSD/Windows 10/MS Office 2019/Radeon Vega 3 Graphics), Natural Silver', 54, 'lappp.jpg', 'HP 15s eq0007au 15.6-inch Laptop (3rd Gen Ryzen 3 3200U/4GB/256GB SSD/Windows 10/MS Office 2019/Radeon Vega 3 Graphics), Natural Silver', 'Silver', '950g', 33434, 0, '2020-09-20', 3),
(18, 'Hitman Trilogy (Masterpiece) for pc', 56, 'hit8.jpeg', 'Hitman Trilogy (Masterpiece) for pc', 'N/A', 'CD - 40g', 1099, 0, '2020-09-21', 1),
(19, 'BERKINS Men\'s Mesh AIR Multi-Colored Series Ultralight Sports Jogging Walking Running', 62, 'shoes.jpg', 'Comfortable Movement- Allows the feet to move as naturally as possible, particularly around the toe area where maximum flexibility is required.', 'Black', '150g', 599, 0, '2020-09-30', 0),
(20, 'Vector X Orion Synthetic Indoor Football Shoes (Orange-Black)', 62, 'aho.jpg', 'Indoor Football Shoes\r\nOuter material: Synthetic leather\r\nSole Material: TPU\r\nPadded footbed\r\nCushioned ankle\r\nFull lace fastening\r\nMaterial: Synthetic\r\nIn-box Contents: 1 x Shoes', 'Orange, Black', '185g', 592, 0, '2020-09-30', 0),
(21, 'Centrino Men\'s Brown Formal Shoes - 8 UK/India (42 EU)(9383-002)', 62, '81KIRWY+kJL__UL1500_.jpg', 'Closure: Lace Up\r\nShoe Width: Medium\r\nMaterial: Synthetic\r\nLifestyle: Business\r\nProduct Type: Men\'s Formal Shoes\r\nClosure Type: Lace-Up\r\nWarranty Type: Manufacturer', 'Brown', '250g', 880, 0, '2020-09-30', 0),
(22, 'Samsung Galaxy M21 (Midnight Blue, 4GB RAM, 64GB Storage)', 61, '61gQ5dO4b1L__SL1200_.jpg', 'Triple Camera Setup - 48MP (F2.0) Main Camera +8MP (F2.2) Ultra Wide Camera +5MP(F2.2) Depth Camera and 20MP (F2.2) front facing Punch Hole Camera\r\n6.4-inch(16.21 centimeters) Super Amoled - Infinity U Cut Display , FHD+ Resolution (2340 x 1080) , 404 ppi', 'Blue', '400g', 13999, 0, '2020-09-30', 0),
(23, 'OnePlus Nord 5G (Gray Onyx, 12GB RAM, 256GB Storage)', 61, '719CgfLcqNL__SL1500_.jpg', '48MP+8MP+5MP+2MP quad rear camera with 1080P Video at 30/60 fps, 4k 30fps | 32MP+8MP front dual camera with 4K video capture at 30/60 fps and 1080 video capture at 30/60 fps\r\n6.44-inch 90Hz fluid Amoled display with 2400 x 1080 pixels resolution | 408ppi\r', 'Gray Onyx', '120g', 29999, 0, '2020-09-30', 0),
(24, 'Phenomenal Round Boys and Girl\'s Sunglasses (Blue)', 59, '51nhBGsC1QL__UL1100_.jpg', 'Size: standard ( Free Size)\r\nGlasses are ideal for day vision\r\nUv protection: 100%, light weight sunglass\r\nEngraved design, Premium finish\r\nFull Frame Sunglasses With Box', 'Sky Blue', '50g', 99, 0, '2020-09-30', 0),
(25, 'Ivonne UniBody Lens Design Mirror Unisex Sunglasses (Blue)', 59, '51nBOZIDtaL__UL1500_.jpg', 'Colour : blue\r\nDepartment : unisex\r\nUniBody Lens Design', 'Blue ', '40g', 299, 0, '2020-09-30', 0),
(26, ' Cosmic Byte GS410 Headphones with Mic and for PS4, Xbox One, Laptop, PC, iPhone and Android Phones (Camo Black)', 55, '41FpCshAvaL.jpg', 'Primary kind of gaming headset, perfect for playing games, listening music, etc\r\nSoft cushion head-pad and ear-pad, as well as adjustable length hinges guarantee hours of gaming comfort\r\nDelivers clear sound and deep bass for real game. Little smart in-li', 'Camo Black', '200g', 1448, 0, '2020-09-30', 0),
(27, 'Tobo Multi Functional Charging Station, Cooling Fan Cooler, PSVR Glasses Holder Bracket for Playstation PS VR Headset,', 56, '612iaphrMML__SL1000_.jpg', 'Designed for Sony PS VR /regular PS4 / PS4 Slim / PS4 Pro - 3 kinds of gaming consoles for a better storage for your bundle of gaming devices and firmly showcase your gaming system in vertical position - Compact space saver and all-in-one desk organizer, ', 'Black', 'N/A', 4999, 0, '2020-09-30', 0),
(28, 'Lenovo Ideapad Slim 3 AMD Athlon Silver 3050U 15.6 HD Thin and Light Laptop (4GB/1TB HDD/Windows/Office/Platinum Grey/1.85Kg), 81W100HKIN', 54, '71a8cjhHbPL__SL1500_.jpg', 'Processor: AMD Athlon Silver 3050U | Speed: 2.3 GHz (Base) - 3.2 GHz (Max) | 2 Cores | 4MB Cache\r\nOS: Pre-Loaded Windows 10 Home with Lifetime Validity\r\nPre-Installed: MS Office Home and Student 2019\r\nMemory and Storage: 4GB RAM DDR4, Upgradable up to 12G', 'Silver', '450g', 32490, 0, '2020-09-30', 0),
(29, 'ASUS TUF Gaming A15 Laptop 15.6\" FHD AMD Ryzen 7 4800H, GTX 1650 4GB GDDR6 Graphics (16GB RAM/1TB HDD + 256GB NVMe SSD/Windows 10/Bonfire Black/2.30 Kg), FA506IH-BQ182T', 54, 'A1A2yQlAXCL__SL1500_.jpg', 'Processor: AMD Ryzen 7 4800H Processor 2.9 GHz (8MB Cache, up to 4.2 GHz, 8 Cores, 16 Threads)\r\nMemory & Storage: 16GB (2x 8GB) DDR4 3200MHz Dual Channel RAM, Upgradeable up to 32GB using 2x SO-DIMM Slot| Storage: 1TB 5400RPM 2.5\" SATA HDD + 256GB M.2 NVM', 'Bonfire Black', '550g', 76837, 0, '2020-09-30', 0),
(30, 'ASUS ROG Zephyrus G14, 14\" FHD 120Hz, Ryzen 5 4600HS, GTX 1650Ti 4GB GDDR6 Graphics, Gaming Laptop (8GB/512GB SSD/MS Office 2019/Windows 10/Moonlight White/Anime Matrix/1.7 Kg), GA401II-HE127TS', 54, '91Oam4MucXL__SL1500_.jpg', 'Processor: AMD Ryzen 5 4600HS Processor, 3.0 GHz (8MB Cache, up to 4.0 GHz, 6 Cores, 12 Threads)\r\nMemory: 8GB DDR4 3200MHz onboard RAM, Upgradeable up to 24GB using 1x SO-DIMM Slot with | Storage: 512GB M.2 NVMe PCIe 3.0 SSD\r\nGraphics: Dedicated NVIDIA Ge', 'Moonlight White', '360g', 108351, 0, '2020-09-30', 0),
(31, 'ASUS ProArt StudioBook Pro 15 Mobile Workstation Laptop, 15.6” UHD NanoEdge Bezel, Intel Core i7-9750H, 48GB DDR4, 2TB PCIe SSD, Nvidia Quadro RTX 5000, Windows 10 Pro, W500G5T-XS77, Star Grey', 54, '81yJcGQ3nNL__SL1500_.jpg', '15. 6\'\' UHD (3840*2160) NanoEdge matte display; 100% Adobe RGB; Pantone validated color accuracy; Delta E <1. 5\r\nIntel Core i7-9750H (12M Cache, up to 4. 5GHz) with Windows 10 Professional\r\nNVIDIA Quadro RTX 5000 with 16GB GDDR6 VRAM - RTX Studio Ready\r\nF', 'Star Grey', '550g', 544868, 0, '2020-09-30', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sold_product`
--

CREATE TABLE `sold_product` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sold_product`
--

INSERT INTO `sold_product` (`id`, `product_id`) VALUES
(8, 16),
(9, 6),
(10, 12),
(11, 8),
(12, 17),
(13, 12),
(14, 12),
(15, 12),
(16, 16),
(17, 6),
(18, 12),
(19, 8),
(20, 7),
(21, 9),
(22, 7),
(23, 9),
(24, 7),
(25, 9),
(26, 7),
(27, 9),
(28, 7),
(29, 9),
(30, 13),
(31, 14),
(32, 12),
(33, 5),
(34, 5);

-- --------------------------------------------------------

--
-- Table structure for table `temp_address`
--

CREATE TABLE `temp_address` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `register_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `email`, `mobile`, `password`, `address`, `register_date`) VALUES
(1, 'Ankit Chouhan', 'ankitchouhan654@gmail.com', '2147483647', '1234567', '12, Namdarpura Ujjain', '2020-09-22'),
(2, 'Vinay Soni', 'infotech@gmail.com', '2147483647', '123456789', '59, Rishi Nagar Ujjain 456001', '2020-09-22'),
(3, 'Deepak Chouhan', 'admin123@gmail.com', '8225994767', '1234567', 'Bhawarkua Indore 456001', '2020-09-22'),
(5, 'Sudanshu Pandey', 'pandey12@gmail.com', '8225994767', '1234567', 'Desai Nagar Chitrkut,456006', '2020-09-30'),
(7, 'Anshul Jain', 'jain1111@gmail.com', '8225994767', '1234567', 'Vijay nagar Indore 456006', '2020-09-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sold_product`
--
ALTER TABLE `sold_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_address`
--
ALTER TABLE `temp_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `sold_product`
--
ALTER TABLE `sold_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `temp_address`
--
ALTER TABLE `temp_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
